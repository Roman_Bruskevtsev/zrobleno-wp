'use strict';
var themeName   = 'zrobleno',
    gulp        = require('gulp'),
    watch       = require('gulp-watch'),
    sass        = require('gulp-sass'),
    cssmin      = require('gulp-cssmin'),
    babel       = require('gulp-babel'),
    uglify      = require('gulp-uglify-es').default,
    rename      = require('gulp-rename'),
    concat      = require('gulp-concat'),
    pipeline    = require('readable-stream').pipeline,
    buildFolder = themeName,
    scriptsArray= [
        themeName + '/assets/js/jquery.mask.js',
        themeName + '/assets/js/swiper.js',
        themeName + '/assets/js/select2.full.js',
        themeName + '/assets/js/jquery.paroller.js',
        // themeName + '/assets/js/mousewheel-smoothscroll.js',
        themeName + '/assets/js/masonry.pkgd.js',
        themeName + '/assets/js/aos.js',
        themeName + '/assets/js/general.js'
    ];

gulp.task('styles',  function () {
    return gulp.src(themeName + '/assets/sass/**/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest(buildFolder + '/assets/css'))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(buildFolder + '/assets/css/'));    
});

gulp.task('scripts', function () {
    return gulp.src(scriptsArray).pipe(concat('all.js'))
            .pipe(rename({suffix: '.min'}))
            .pipe(uglify())
            .pipe(gulp.dest(buildFolder + '/assets/js/'));
});

gulp.task('theme', function() {
    return gulp.src(buildFolder + '/**/*')
            .pipe(gulp.dest(themeName));
});

gulp.task('default', gulp.series(
    gulp.parallel(
    'styles',
    'scripts'
    )
));

gulp.task('watch', function () {
    gulp.watch(themeName + '/assets/sass/**/*.scss', gulp.series('styles'));

    gulp.watch(scriptsArray, gulp.series('scripts'));
});
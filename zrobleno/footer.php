<?php
/**
 *
 * @package WordPress
 * @subpackage Zrobleno
 * @since 1.0
 * @version 1.0
 */
$languages = pll_the_languages(array('raw'=>1));
?>
    </main>
    <footer>
    	<div class="container">
            <div class="row">
                <div class="col">
                    <?php
                    if( $languages ){
                        $current_lang = $lang_list = '';
                        foreach ( $languages as $lang ) {
                            if( $lang['current_lang'] ){
                                $current_lang = $lang['name'];
                            } else {
                                $lang_list .= '<li><a href="'.$lang['url'].'">'.$lang['name'].'</a></li>';
                            }
                        } ?>
                        <div class="language__switcher d-block d-lg-none text-right">
                            <ul>
                                <?php echo $lang_list; ?>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
            </div>
    		<div class="row">
    			<div class="col-md-4">
    				<div class="contact__block float-right">
					<?php if( get_field('phone_number', 'option') ) { ?>
						<div><a href="tel:<?php the_field('phone_number', 'option'); ?>" class="phone"><?php the_field('phone_number', 'option'); ?></a></div>
                    <?php }
                    if( get_field('call_back_label', 'option') ) { ?>
                        <div><button id="call-popup-footer" class="btn transparent"><span><?php the_field('call_back_label', 'option'); ?></span></button></div>
                    <?php }
                    if( get_field('email', 'option') ) { ?>
                    	<div><a href="mailto:<?php the_field('email', 'option'); ?>" class="email"><?php the_field('email', 'option'); ?></a></div>
                    <?php } ?>
    				</div>
    			</div>
    			<div class="col-md-6">
    				<div class="row">
					<?php if ( is_active_sidebar( 'footer-1' ) ) { ?>
						<div class="col-md-7"><?php dynamic_sidebar( 'footer-1' ); ?></div>
					<?php } ?>
					<?php if ( is_active_sidebar( 'footer-2' ) ) { ?>
						<div class="col-md-5"><?php dynamic_sidebar( 'footer-2' ); ?></div>
					<?php } ?>
						<div class="col-md-12"><div class="small__line"></div></div>
    				</div>
    				<?php
    				$facebook = get_field('facebook', 'option');
    				$youtube = get_field('youtube', 'option');
    				?>
    				<div class="row">
    					<div class="col-md-7">
    						<?php if( $facebook ) { ?>
    						<div class="social__block">
    							<a href="<?php echo $facebook['url']; ?>" target="_blank">
    								<div class="icon facebook"></div>
    								<span><?php _e('Go to', 'zrobleno'); ?></span>
    							</a>
    							<?php if( $facebook['text'] ) { ?>
    							<div class="text"><p><?php echo $facebook['text']; ?></p></div>
    							<?php }
    							if( $facebook['notice'] ) { ?>
    							<div class="notice"><p><?php echo $facebook['notice']; ?></p></div>
    							<?php } ?>
    						</div>
    						<?php } ?>
    					</div>
    					<div class="col-md-5">
    						<?php if( $youtube ) { ?>
    						<div class="social__block">
    							<a href="<?php echo $youtube['url']; ?>" target="_blank">
    								<div class="icon youtube"></div>
    								<span><?php _e('View', 'zrobleno'); ?></span>
    							</a>
    							<?php if( $youtube['text'] ) { ?>
    							<div class="text"><p><?php echo $youtube['text']; ?></p></div>
    							<?php }
    							if( $youtube['notice'] ) { ?>
    							<div class="notice"><p><?php echo $youtube['notice']; ?></p></div>
    							<?php } ?>
    						</div>
    						<?php } ?>
    					</div>
    				</div>
				</div>
    			<div class="col-md-2">
				<?php
				if( $languages ){
					$current_lang = $lang_list = '';
                    foreach ( $languages as $lang ) {
                        if( $lang['current_lang'] ){
                            $current_lang = $lang['name'];
                        } else {
                            $lang_list .= '<li><a href="'.$lang['url'].'">'.$lang['name'].'</a></li>';
                        }
                    } ?>
                    <div class="language__switcher d-none d-lg-block text-right">
                        <ul>
                            <?php echo $lang_list; ?>
                        </ul>
                    </div>
            	<?php }
            	$images = get_field('paying_systems_icons', 'option');

            	if( $images ) { ?>
            		<div class="paying__images text-right">
        			<?php foreach ( $images as $image ) { ?>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
    			    <?php } ?>
            		</div>
            	<?php } ?>
    			</div>
    		</div>
    	</div>
    </footer>
    <div class="popup__wrapper"></div>
    <?php
    get_template_part( 'template-parts/popup' );
    get_template_part( 'template-parts/package-popup' );

    wp_footer(); ?>

    <script>
        (function(w, d, s, h, id) {
            w.roistatProjectId = id; w.roistatHost = h;
            var p = d.location.protocol == "https:" ? "https://" : "http://";
            var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
            var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
        })(window, document, 'script', 'cloud.roistat.com', '03599fb1efe97dc08378ddfc80729add');
    </script>
    <script>
        jQuery(document).ready(function($) {
            let phones_input = $('[name=tel-full]');
            if(phones_input.length > 0){
                phones_input.each(function (index) {
                    $(this).attr('autocomplete', 'on');
                });
            }
        });
    </script>

    <!-- <script>var amo_social_button = {id: "21867", hash: "95bbd542e881c532e45a0eb30b4c8b6167f2219c1edb528a2776662903ecc18f", locale: "ru", setMeta: function(params) {this.params = this.params || []; this.params.push(params);}};</script><script id="amo_social_button_script" async="async" src="https://gso.amocrm.ru/js/button.js"></script> -->

    <script>(function(a,m,o,c,r,m){a[m]={id:"21867",hash:"95bbd542e881c532e45a0eb30b4c8b6167f2219c1edb528a2776662903ecc18f",locale:"ru",setMeta:function(p){this.params=(this.params||[]).concat([p])}};a[o]=a[o]||function(){(a[o].q=a[o].q||[]).push(arguments)};var d=a.document,s=d.createElement('script');s.async=true;s.id=m+'_script';s.src='https://gso.amocrm.ru/js/button.js?1605875397';d.head&&d.head.appendChild(s)}(window,0,'amoSocialButton',0,0,'amo_social_button'));</script>

    <?php
    $site =  getenv('HTTP_HOST') . getenv('REQUEST_URI');
    $tid = (isset($_SESSION['GATID'])) ? $_SESSION['GATID'] : NULL;
    $cid = (isset($_COOKIE['_ga'])) ?  preg_replace("/^.+\.(.+?\..+?)$/", "\\1", @$_COOKIE['_ga']) :  NULL;
    if (isset($_SESSION['wp_marker_utm'])) {
        parse_str($_SESSION['wp_marker_utm'], $utm);
    }
    $utm_source = ( isset($utm['utm_source']) ) ? $utm['utm_source'] : '';
    $utm_medium = ( isset($utm['utm_medium']) ) ? $utm['utm_medium'] : '';
    $utm_campaign = ( isset($utm['utm_campaign']) ) ? $utm['utm_campaign'] : '';
    $utm_content = ( isset($utm['utm_content']) ) ? $utm['utm_content'] : '';
    $utm_term = ( isset($utm['utm_term']) ) ? $utm['utm_term'] : '';
    $ga_utm =  json_encode([
        'ga' => [
            'trackingId' => (isset($_SESSION['GATID'])) ? $_SESSION['GATID'] : '',
            'clientId' => (isset($_COOKIE['_ga'])) ?  preg_replace("/^.+\.(.+?\..+?)$/", "\\1", @$_COOKIE['_ga']) :  '',
        ],
        'utm' => [
            'source' => $utm_source,
            'medium' => $utm_medium,
            'content' => $utm_content,
            'campaign' => $utm_campaign,
            'term' => $utm_term,
        ],
        'data_source' => 'form'
    ]);

    $roistat = ( isset($_COOKIE['roistat_visit']) ) ? $_COOKIE['roistat_visit'] : '';
     ?>

     <script>
        setTimeout(
            amo_social_button.setMeta({
                lead: {
                    name: 'Amocrm Button',
                    custom_fields: [
                    {
                        id: 453577, values:
                        [
                        {
                            value: "<?= $site ?>"
                        }
                        ]
                    },
                    {
                        id: 448683, values:
                        [
                        {
                            value: "<?= $utm_source ?>"
                        }
                        ]
                    },
                    {
                        id: 447955, values:
                        [
                        {
                            value: "<?= $utm_source ?>"
                        }
                        ]
                    },
                    {
                        id: 448685, values:
                        [
                        {
                            value: "<?= $utm_medium ?>"
                        }
                        ]
                    },
                    {
                        id: 447957, values:
                        [
                        {
                            value: "<?= $utm_medium ?>"
                        }
                        ]
                    },
                    {
                        id: 448687, values:
                        [
                        {
                            value: "<?= $utm_campaign ?>"
                        }
                        ]
                    },
                    {
                        id: 447959, values:
                        [
                        {
                            value: "<?= $utm_campaign ?>"
                        }
                        ]
                    },
                    {
                        id: 447961, values:
                        [
                        {
                            value: "<?= $utm_content ?>"
                        }
                        ]
                    },
                    {
                        id: 447963, values:
                        [
                        {
                            value: "<?= $utm_term ?>"
                        }
                        ]
                    },
                    {
                        id: 448585, values:
                        [
                        {
                            value: '<?= $ga_utm ?>'
                        }
                        ]
                    },
                    {
                        id: 448293, values:
                        [
                        {
                            value: "<?= $roistat ?>"
                        }
                        ]
                    }
                    ]
                }
            }), 5000
        );
    </script>
<style>
    .phone-select{
        display: flex;
    }
    .phone-select .wpcf7-form-control-wrap.tel-code{
        margin-right: 5px;
    }
    .phone-select .wpcf7-form-control-wrap.tel-code .select2-container--default .select2-selection--single .select2-selection__rendered{
        height: 44px;
        border: 2px solid #FFF;
        border-radius: 1px;
        background: rgba(255,255,255,.12);
        font-size: 16px;
        line-height: 20px;
        padding: 0 16px;
        color: #fff;
        display: flex;
        align-items: center;
    }
    .phone-select .wpcf7-form-control-wrap.tel-code .select2-container{
        width: 80px!important;
        height: 44px;
    }
    .phone-select .wpcf7-form-control-wrap.tel-code .select2-container--default .select2-selection--single {
        background-color: transparent;
        border: none;
        border-radius: 0;
    }
    .phone-select .wpcf7-form-control-wrap.tel-code .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 26px;
        position: absolute;
        top: 8px;
        right: 3px;
        width: 20px;
    }
    .phone-select .wpcf7-form-control-wrap.tel-code .select2-container--default .select2-selection--single .select2-selection__arrow b{
        border-color: #fff transparent transparent;
    }
    .phone-select.phone-select-dark .wpcf7-form-control-wrap.tel-code .select2-container--default .select2-selection--single .select2-selection__rendered{
        border: 2px solid rgba(54,91,120,.4);
        background: #fff;
        color: #111;
    }
    .phone-select.phone-select-dark .wpcf7-form-control-wrap.tel-code .select2-container--default .select2-selection--single .select2-selection__arrow b{
        border-color: #111 transparent transparent;
    }
    #select2-tel-code-*{
        background-color: #648197;
        border: 2px solid #fff;
        border-radius: 0;
    }
    .phone-select.phone-select-dark .form__field{
        border: 2px solid rgba(54,91,120,.4);
        background: #fff;
        color: #111;
    }
    .phone-select-label label .placeholder{
        margin-bottom: 1px;
    }
    .select2-container.select2-container--open{
        z-index: 99999999;
    }
    @media (max-width:767.98px) {
        .field__group.half .group.phone-select-label {
            width: 100%;
        }
    }
</style>
</body>
</html>
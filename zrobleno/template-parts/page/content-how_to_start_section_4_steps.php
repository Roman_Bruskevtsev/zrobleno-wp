<section class="how__to__start background__grey">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="title" data-aos="fade-left" data-aos-duration="600">
					<h2><?php the_sub_field('title'); ?></h2>
					<?php if( get_sub_field('text') ) { ?><p><?php the_sub_field('text'); ?></p><?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>
		<div class="row">
			<div class="col">
				<div class="steps__block steps__4">
					<div class="content" data-aos="fade-up" data-aos-duration="600">
					<?php 
					$step_1 = get_sub_field('step_1');

					if( $step_1 ){ ?>
						<div class="step__block">
							<?php if($step_1['icon']) { ?>
							<div class="icon">
								<img src="<?php echo $step_1['icon']['url']; ?>" alt="<?php echo $step_1['icon']['title']; ?>">
							</div>
							<?php }	
							if($step_1['text']) { ?>
							<p><?php echo $step_1['text']; ?></p>
							<?php }
							if( $step_1['secure'] ) { ?>
								<div class="secure"><?php _e('Secure', 'zrobleno'); ?></div>
							<?php } ?>
						</div>
					<?php }
					
					$step_2 = get_sub_field('step_2');

					if( $step_2 ){ ?>
						<div class="step__block">
							<?php if($step_2['icon']) { ?>
							<div class="icon">
								<img src="<?php echo $step_2['icon']['url']; ?>" alt="<?php echo $step_2['icon']['title']; ?>">
							</div>
							<?php }	
							if($step_2['text']) { ?>
							<p><?php echo $step_2['text']; ?></p>
							<?php }
							if( $step_2['secure'] ) { ?>
								<div class="secure"><?php _e('Secure', 'zrobleno'); ?></div>
							<?php } ?>
						</div>
					<?php }
					$step_3 = get_sub_field('step_3');

					if( $step_3 ){ ?>
						<div class="step__block">
							<?php if($step_3['icon']) { ?>
							<div class="icon">
								<img src="<?php echo $step_3['icon']['url']; ?>" alt="<?php echo $step_3['icon']['title']; ?>">
							</div>
							<?php }	
							if($step_3['text']) { ?>
							<p><?php echo $step_3['text']; ?></p>
							<?php }
							if( $step_3['secure'] ) { ?>
								<div class="secure"><?php _e('Secure', 'zrobleno'); ?></div>
							<?php } ?>
						</div>
					<?php }
					$step_4 = get_sub_field('step_4');

					if( $step_4 ){ ?>
						<div class="step__block">
							<?php if($step_4['icon']) { ?>
							<div class="icon">
								<img src="<?php echo $step_4['icon']['url']; ?>" alt="<?php echo $step_4['icon']['title']; ?>">
							</div>
							<?php }	
							if($step_4['text']) { ?>
							<p><?php echo $step_4['text']; ?></p>
							<?php }
							if( $step_4['secure'] ) { ?>
								<div class="secure"><?php _e('Secure', 'zrobleno'); ?></div>
							<?php } ?>
						</div>
					<?php } ?>
					</div>
					<?php 
					$form = get_sub_field('form');
					if( $form ) { ?>
					<div class="form__block" data-aos="fade-left" data-aos-duration="600">
						<?php if( $form['title'] ) { ?><h5 class="text-center"><?php echo $form['title']; ?></h5><?php } ?>
						<?php echo do_shortcode($form['form_shortcode']); ?>
						<?php if( $form['notice_text'] ) { ?><p><?php echo $form['notice_text']; ?></hp><?php } ?>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
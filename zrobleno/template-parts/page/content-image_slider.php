<section class="image__slider__section">
	<div class="container-fluid nopadding">
		<div class="row">
			<div class="col">
				<?php
				$slider = get_sub_field('images'); 
				if( $slider ) { ?>
				<div class="image__slider swiper-container" data-aos="fade-up" data-aos-duration="600">
					<div class="swiper-wrapper">
					<?php foreach( $slider as $slide ) { ?>
						<div class="swiper-slide">
							<img src="<?php echo $slide['url']; ?>" alt="<?php echo $slide['title']; ?>">
						</div>
					<?php } ?>
					</div>
					<div class="swiper-pagination"></div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
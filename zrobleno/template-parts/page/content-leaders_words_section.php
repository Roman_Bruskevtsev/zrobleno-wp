<section class="leaders__words__section">
	<div class="container">
		<div class="row">
			<?php if( get_sub_field('title') ) { ?>
			<div class="col">
				<div class="title" data-aos="fade-left" data-aos-duration="600">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
			<?php } ?>
		</div>
		<?php 
		$top = get_sub_field('top_row'); 
		if( $top ) { ?>
		<div class="row content__row">
			<div class="col-lg-6">
				<div class="image__block left" data-aos="fade-right" data-aos-duration="600">
					<?php if( $top['image'] ) { ?>
						<img src="<?php echo $top['image']['url']; ?>" alt="<?php echo $top['image']['title']; ?>">
					<?php } ?>
					<div class="position">
						<?php if( $top['name'] ) { ?><h4><?php echo $top['name']; ?></h4><?php } ?>
						<?php if( $top['position'] ) { ?><p><?php echo $top['position']; ?></p><?php } ?>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<?php if( $top['text'] ) { ?><div class="text" data-aos="fade-left" data-aos-duration="600"><?php echo $top['text']; ?></div><?php } ?>
			</div>
		</div>
		<?php }
		$bottom = get_sub_field('bottom_row'); 
		if( $bottom ) { ?>
		<div class="row">
			<div class="col-lg-6 text__column">
				<?php if( $bottom['text'] ) { ?><div class="text" data-aos="fade-right" data-aos-duration="600"><?php echo $bottom['text']; ?></div><?php } ?>
			</div>
			<div class="col-lg-6 image__column">
				<div class="image__block right" data-aos="fade-left" data-aos-duration="600">
					<?php if( $bottom['image'] ) { ?>
						<img src="<?php echo $bottom['image']['url']; ?>" alt="<?php echo $bottom['image']['title']; ?>">
					<?php } ?>
					<div class="position text-right">
						<?php if( $bottom['name'] ) { ?><h4><?php echo $bottom['name']; ?></h4><?php } ?>
						<?php if( $bottom['position'] ) { ?><p><?php echo $bottom['position']; ?></p><?php } ?>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>
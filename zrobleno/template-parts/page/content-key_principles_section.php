<section class="key__principles__section background__dark">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="title" data-aos="fade-left" data-aos-duration="600">
					<h2><?php the_sub_field('title'); ?></h2>
					<?php if( get_sub_field('subtitle') ) { ?><p><?php the_sub_field('subtitle'); ?></p><?php } ?>
				</div>
			</div>
		</div>
		<?php } 
		$principles = get_sub_field('principles'); 
		if( $principles ) { ?>
		<div class="row">
		<?php foreach ( $principles as $principle ) { ?>
			<div class="col-md-6 col-lg-4 principle__cell">
				<div class="principle" data-aos="fade-up" data-aos-duration="600">
					<?php if( $principle['icon'] ) { ?>
					<div class="icon text-center">
						<?php if( $principle['icon'] ) { ?>
							<img src="<?php echo $principle['icon']['url']; ?>" alt="<?php echo $principle['icon']['title']; ?>">
						<?php } ?>
					</div>
					<?php } 
					if( $principle['text'] ) { ?>
					<div class="text"><?php echo $principle['text']; ?></div>
					<?php } ?>
				</div>
			</div>
		<?php } ?>
		</div>
		<?php } ?>
	</div>
</section>
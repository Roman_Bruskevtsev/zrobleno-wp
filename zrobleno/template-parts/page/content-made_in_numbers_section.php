<section class="made__numbers__section background__grey">
	<div class="container">
		<?php 
		$top = get_sub_field('top_row');
		if( $top['title'] ) { ?>
		<div class="row">
			<div class="col">
				<div class="title" data-aos="fade-left" data-aos-duration="600">
					<h3><?php echo $top['title']; ?></h3>
				</div>
			</div>
		</div>
		<?php }
		if( $top['blocks'] ) { ?>
		<div class="row">
			<?php foreach ( $top['blocks'] as $block ) { ?>
			<div class="col-sm-12 col-md-4 line__block">
				<div class="number__block" data-aos="fade-up" data-aos-duration="600">
					<?php if( $block['icon'] ) { ?>
					<div class="icon">
						<img src="<?php echo $block['icon']['url']; ?>" alt="<?php echo $block['icon']['title']; ?>">
					</div>
					<?php }
					if( $block['title'] ) { ?>
					<h2 class="dark-blue"><?php echo $block['title']; ?></h2>
					<?php } 
					if( $block['subtitle'] ) { ?>
					<h5 class="dark-blue"><?php echo $block['subtitle']; ?></h5>
					<?php } 
					if( $block['text'] ) { ?>
					<p><?php echo $block['text']; ?></p>
					<?php } ?>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="row">
			<div class="col">
				<div class="line"></div>
			</div>
		</div>
		<?php }
		$bottom = get_sub_field('bottom_row');
		if( $bottom['title'] ) { ?>
		<div class="row">
			<div class="col">
				<div class="title" data-aos="fade-left" data-aos-duration="600">
					<h3><?php echo $bottom['title']; ?></h3>
				</div>
			</div>
		</div>
		<?php } 
		if( $bottom['blocks'] ) { ?>
		<div class="row">
			<?php foreach ( $bottom['blocks'] as $block ) { ?>
			<div class="col-md-4">
				<div class="icon__block" data-aos="fade-up" data-aos-duration="600">
					<?php if( $block['icon'] ) { ?>
					<img src="<?php echo $block['icon']['url']; ?>" alt="<?php echo $block['icon']['title']; ?>">
					<?php } 
					if( $block['text'] ) { ?>
					<div class="text">
						<p><?php echo $block['text']; ?></p>
					</div>
					<?php } ?>
				</div>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</section>

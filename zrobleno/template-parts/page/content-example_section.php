<section class="example__section small__padding background__grey">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-10">
				<div class="title" data-aos="fade-left" data-aos-duration="600">
					<h1><?php the_sub_field('title'); ?></h1>
				</div>
			</div>
		</div>
		<?php } 
		$style = get_sub_field('style') == '0' ? '' : ' no__subtitle';
		?>
		<div class="row justify-content-center">
			<div class="col-md-10 col-lg-8">
			<?php $tabs = get_sub_field('tabs');
			if( $tabs ){ ?>
				<div class="tabs" data-aos="fade-up" data-aos-duration="600">
					<div class="navigation">
					<?php 
					$i = 0;
					foreach ( $tabs as $tab ) { 
						$class = ( $i == 0 ) ? ' active' : ''; ?>
						<div class="tab<?php echo $class; ?>"><?php echo $tab['title']; ?></div>
					<?php $i++; } ?>
					</div>
					<div class="content">
					<?php 
					$i = 0;
					foreach ( $tabs as $tab ) { 
						$class = ( $i == 0 ) ? ' active' : ''; ?>
						<div class="tab<?php echo $class; ?>">
						<?php $slider = $tab['examples']; 
						if( $slider ) { ?>
							<div class="tab__slider swiper-container<?php echo $style; ?>">
								<div class="swiper-wrapper">
								<?php foreach ( $slider as $slide ) { ?>
									<div class="swiper-slide">
										<div class="example">
											<?php if( $slide['icon'] ) { ?>
											<div class="icon">
												<img src="<?php echo $slide['icon']['url']; ?>" alt="<?php echo $slide['icon']['title']; ?>">
											</div>
											<?php } 
											if( get_sub_field('style') == '0' ) { ?>
											<div class="content">
												<?php if( $slide['company_type'] ) { ?><h5><?php echo $slide['company_type']; ?></h5><?php } ?>
												<?php if( $slide['name'] ) { ?><p><b><?php echo $slide['name']; ?></b></p><?php } ?>
												<?php if( $slide['text'] ) { ?><p><?php echo $slide['text']; ?></p><?php } ?>
												<?php if( $slide['price'] ) { ?><h6 class="dark-blue"><?php echo $slide['price']; ?></h6><?php } ?>
											</div>
											<?php } else { ?>
											<div class="ex__title">
												<?php if( $slide['company_type'] ) { ?><h5><?php echo $slide['company_type']; ?></h5><?php } ?>
											</div>
											<div class="content">
												<?php if( $slide['text'] ) { ?><p><?php echo $slide['text']; ?></p><?php } ?>
												<?php if( $slide['price'] ) { ?><h6 class="dark-blue"><?php echo $slide['price']; ?></h6><?php } ?>
											</div>
											<?php } ?>
										</div>
									</div>
								<?php } ?>
								</div>
							</div>
							<div class="swiper-button-next"></div>
							<div class="swiper-button-prev"></div>
							<div class="swiper-pagination"></div>
						<?php } ?>
						</div>
					<?php $i++; } ?>
					</div>
				</div>
			<?php } ?>
			</div>
			<div class="col-lg-4">
				<?php
				$form = get_sub_field('form');
				if( $form ) { ?>
				<div class="form__block d-none d-lg-block" data-aos="fade-up" data-aos-duration="600">
					<?php if( $form['title'] ) { ?><h5><?php echo $form['title']; ?></h5><?php } ?>
					<?php echo do_shortcode($form['form_shortcode']); ?>
					<?php if( $form['notice_text'] ) { ?><p><?php echo $form['notice_text']; ?></hp><?php } ?>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
<?php if( $form ) { ?>
<section class="form__section background__dark d-block d-lg-none">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-sm-7">
				<div class="form__block" data-aos="fade-up" data-aos-duration="600">
					<?php if( $form['title'] ) { ?><h5 class="text-left"><?php echo $form['title']; ?></h5><?php } ?>
					<?php echo do_shortcode($form['form_shortcode']); ?>
					<?php if( $form['notice_text'] ) { ?><p><?php echo $form['notice_text']; ?></hp><?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>
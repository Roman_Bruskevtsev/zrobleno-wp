<section class="posts__section__webinars">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="title" data-aos="fade-left" data-aos-duration="600">
					<?php if( get_sub_field('title_url') ) { ?><a href="<?php echo get_sub_field('title_url'); ?>" class="link"><?php } ?>
						<h2><?php the_sub_field('title'); ?></h2>
					<?php if( get_sub_field('title_url') ) { ?></a><?php } ?>
				</div>
			</div>
		</div>
		<?php } 
		$posts = get_sub_field('choose_post_to_show'); 
		if( $posts ) {
			$args = array(
				'posts_per_page' 	=> -1,
				'post_type'			=> 'post',
				'post__in'			=> $posts 
			);
			$query = new WP_Query($args);

			if ( $query->have_posts() ) { ?>
				<div class="row">
					<div class="col">
					<?php while ( $query->have_posts() ) { $query->the_post();
						get_template_part( 'template-parts/post/content', 'webinar-wide' );
					} ?>
					</div>
				</div>
			<?php }
			wp_reset_postdata();
		} ?>
	</div>
</section>
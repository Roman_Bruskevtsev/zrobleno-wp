<section class="contacts__section">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="content_block" data-aos="fade-left" data-aos-duration="600">
					<h1><?php the_sub_field('title'); ?></h1>
					<div class="phone__row">
					<?php if( get_sub_field('phone') ) { ?>
						<a class="phone" href="tel:<?php the_sub_field('phone'); ?>"><?php the_sub_field('phone'); ?></a>
					<?php } 
					if( get_sub_field('viber') ) { ?>
						<a class="viber" href="<?php the_sub_field('viber'); ?>"></a>
					<?php } 
					if( get_sub_field('telegram') ) { ?>
						<a class="telegram" href="<?php the_sub_field('telegram'); ?>"></a>
					<?php } 
					if( get_sub_field('messenger') ) { ?>
						<a class="messenger" href="<?php the_sub_field('messenger'); ?>"></a>
					<?php } 
					if( get_sub_field('email') ) { ?>
						<a class="email" href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a>
					<?php } ?>
					</div>
					<?php if( get_sub_field('timetable') ) { ?>
					<div class="timetable">
						<?php the_sub_field('timetable'); ?>
					</div>
					<?php } ?>
					<?php if( get_sub_field('text') ) { ?>
					<div class="text">
						<?php the_sub_field('text'); ?>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<?php 
	$map = get_sub_field('map'); 
	if( $map ) { ?>
	<div class="container-fluid nopadding">
		<div class="row">
			<div class="col">
				<div class="map__wrapper">
					<div id="google__map" class="map" data-lat="<?php echo $map['latitude']; ?>" data-lng="<?php echo $map['longitude']; ?>" data-zoom="<?php echo $map['zoom']; ?>" data-marker="<?php echo $map['marker']['url']; ?>"></div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
</section>
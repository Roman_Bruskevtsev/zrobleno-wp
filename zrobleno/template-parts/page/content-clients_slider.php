<section class="clients__slider__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="title" data-aos="fade-left" data-aos-duration="600">
					<h3><?php the_sub_field('title'); ?></h3>
				</div>
			</div>
		</div>
		<?php } 
		$slider = get_sub_field('clients'); 
		if( $slider ) { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="clients__slider swiper-container">
					<div class="swiper-wrapper">
					<?php foreach ( $slider as $slide ) { ?>
						<div class="swiper-slide">
							<?php if( $slide['logo'] ) { ?>
							<div class="logo">
								<img src="<?php echo $slide['logo']['url']; ?>" alt="<?php echo $slide['logo']['title']; ?>">
							</div>
							<?php }
							if( $slide['text'] ) { ?>
							<div class="content">
								<?php echo $slide['text']; ?>
							</div>
							<?php } 
							if( $slide['price'] ) { ?>
							<div class="price">
								<p><?php echo $slide['price']; ?></p>
							</div>
							<?php } ?>
						</div>
					<?php } ?>
					</div>
					<div class="swiper-pagination"></div>
					<div class="swiper-button-next"></div>
    				<div class="swiper-button-prev"></div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>
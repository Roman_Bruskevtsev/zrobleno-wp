<section class="social__networks__section background__grey">
	<div class="container">
		<div class="row">
			<?php 
			$socials_block = get_sub_field('facebook');
			if( $socials_block ) { ?>
			<div class="col-md-4">
				<div class="social facebook" data-aos="fade-up" data-aos-duration="600">
					<?php if( $socials_block['image'] ) { ?>
					<div class="image" style="background-image: url(<?php echo $socials_block['image']['url']; ?>);"></div>
					<?php } ?>
					<div class="content">
						<?php if( $socials_block['title'] ) { ?>
						<div class="title"><h5><?php echo $socials_block['title']; ?></h5></div>
						<?php } 
						if( $socials_block['subscribers_text'] ) { ?>
						<div class="details"><?php echo $socials_block['subscribers_text']; ?></div>
						<?php }
						if( $socials_block['link'] ) { ?>
						<div class="text-center">
							<a target="_blank" class="btn secondary" href="<?php echo $socials_block['link']; ?>">
								<span><?php echo $socials_block['button_label']; ?></span>
							</a>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<?php } 
			$socials_block = get_sub_field('telegram');
			if( $socials_block ) { ?>
			<div class="col-md-4">
				<div class="social telegram" data-aos="fade-up" data-aos-duration="600">
					<?php if( $socials_block['image'] ) { ?>
					<div class="image" style="background-image: url(<?php echo $socials_block['image']['url']; ?>);"></div>
					<?php } ?>
					<div class="content">
						<?php if( $socials_block['title'] ) { ?>
						<div class="title"><h5><?php echo $socials_block['title']; ?></h5></div>
						<?php } 
						if( $socials_block['subscribers_text'] ) { ?>
						<div class="details"><?php echo $socials_block['subscribers_text']; ?></div>
						<?php }
						if( $socials_block['link'] ) { ?>
						<div class="text-center">
							<a target="_blank" class="btn secondary" href="<?php echo $socials_block['link']; ?>">
								<span><?php echo $socials_block['button_label']; ?></span>
							</a>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<?php } 
			$socials_block = get_sub_field('youtube');
			if( $socials_block ) { ?>
			<div class="col-md-4">
				<div class="social youtube" data-aos="fade-up" data-aos-duration="600">
					<?php if( $socials_block['image'] ) { ?>
					<div class="image" style="background-image: url(<?php echo $socials_block['image']['url']; ?>);"></div>
					<?php } ?>
					<div class="content">
						<?php if( $socials_block['title'] ) { ?>
						<div class="title"><h5><?php echo $socials_block['title']; ?></h5></div>
						<?php } 
						if( $socials_block['subscribers_text'] ) { ?>
						<div class="details"><?php echo $socials_block['subscribers_text']; ?></div>
						<?php }
						if( $socials_block['link'] ) { ?>
						<div class="text-center">
							<a target="_blank" class="btn secondary" href="<?php echo $socials_block['link']; ?>">
								<span><?php echo $socials_block['button_label']; ?></span>
							</a>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>
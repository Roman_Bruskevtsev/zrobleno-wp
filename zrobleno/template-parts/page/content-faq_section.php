<section class="faq__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="title" data-aos="fade-left" data-aos-duration="600">
					<h3><?php the_sub_field('title'); ?></h3>
				</div>
			</div>
		</div>
		<?php } 
		$faqs = get_sub_field('links'); 
		if($faqs){
			$args = array(
				'posts_per_page' 	=> -1,
				'post_type'			=> 'faq',
				'post__in'			=> $faqs 
			);
			$query = new WP_Query($args); 
			if ( $query->have_posts() ) { ?>
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="links__block">
					<?php while ( $query->have_posts() ) { $query->the_post(); ?>
						<a href="<?php the_permalink(); ?>" data-aos="fade-up" data-aos-duration="600"><?php the_title(); ?></a>
					<?php } ?>
					</div>
				</div>
			</div>
			<?php }	wp_reset_postdata();
		} ?>
	</div>
</section>
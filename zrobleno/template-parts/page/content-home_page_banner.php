<?php 
$background = get_sub_field('background') ? ' style="background-image: url('.get_sub_field('background')['url'].')"' : '';
?>
<section class="home__banner"<?php echo $background; ?>>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-7">
				<div class="content">
					<?php if( get_sub_field('title') ) { ?><div class="title" data-aos="fade-left" data-aos-duration="600"><?php the_sub_field('title'); ?></div><?php } ?>
					<?php if( get_sub_field('text') ) { ?><div class="text d-none d-sm-block" data-aos="fade-left" data-aos-duration="600" data-aos-delay="200"><?php the_sub_field('text'); ?></div><?php } ?>
					<?php if( get_sub_field('timetable') ) { ?><div class="timetable d-none d-sm-block" data-aos="fade-up" data-aos-duration="600" data-aos-delay="400"><p><?php the_sub_field('timetable'); ?></p></div><?php } ?>
				</div>
			</div>
			<?php if( get_sub_field('button_link') ) { ?>
			<div class="col-md-5">
				<div class="button__block float-right d-none d-md-block" data-aos="fade-up" data-aos-duration="600">
					<a href="<?php the_sub_field('button_link'); ?>" class="btn white shadow">
						<span><?php the_sub_field('button_label'); ?></span>
					</a>
					<?php if( get_sub_field('button_text') ) { ?><div class="label"><?php the_sub_field('button_text'); ?></div><?php } ?>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>
<section class="additional__section d-block d-md-none">
	<div class="container-fluid">
		<div class="row">
			<div class="col">
				<div class="bottom__block">
					<?php if( get_sub_field('text') ) { ?>
						<div class="text d-block d-sm-none" data-aos="fade-left" data-aos-duration="600" data-aos-delay="200">
							<?php the_sub_field('text'); ?>
						</div>
					<?php } 
					if( get_sub_field('timetable') ) { ?>
						<div class="timetable d-block d-sm-none" data-aos="fade-up" data-aos-duration="600" data-aos-delay="400"><p><?php the_sub_field('timetable'); ?></p></div>
					<?php } ?>
					<div class="button__block float-left" data-aos="fade-left" data-aos-duration="600">
						<a href="<?php the_sub_field('button_link'); ?>" class="btn blue shadow">
							<span><?php the_sub_field('button_label'); ?></span>
						</a>
						<?php if( get_sub_field('button_text') ) { ?><div class="label"><?php the_sub_field('button_text'); ?></div><?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
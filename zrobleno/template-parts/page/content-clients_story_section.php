<section class="clients__story__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="title" data-aos="fade-left" data-aos-duration="600">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
		<?php }
		if( get_sub_field('testimonial') || get_sub_field('comments') ) { ?>
		<div class="row">
			<?php 
			$testimonial = get_sub_field('testimonial');
			$comments = get_sub_field('comments');
			if( $testimonial ) { 
				$args = array(
					'posts_per_page' 	=> 1,
					'post_type' 		=> 'testimonils',
					'p'					=> $testimonial
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) { while ( $query->have_posts() ) { $query->the_post(); ?>
					<div class="col-lg-7">
						<?php 
						get_template_part( 'template-parts/testimonial/content', 'testimonial-small' ); 
						$link = get_sub_field('testimonials_link');
						if( $link ) { 
							$target = $link['target'] ? ' target="_blank' : ''; ?>
						<a class="text__link" href="<?php echo $link['url']; ?>"<?php echo $target; ?>><?php echo $link['title']; ?></a>
						<?php } ?>
					</div>
				<?php }
				} wp_reset_postdata(); 
			} 
			if( $comments ) { ?>
			<div class="col-lg-5">
				<div class="comments__block">
				<?php foreach ( $comments as $comment ) { 
					$type = $comment['type'] == '0' ? ' google' : ' facebook'; ?>
					<?php if( $comment['comment_url'] ) { ?><a href="<?php echo $comment['comment_url']; ?>" targer="_blank"><?php } ?>
						<div class="comment<?php echo $type; ?>" data-aos="fade-up" data-aos-duration="600">
							<div class="header">
								<?php if( $comment['avatar'] ) { ?>
								<div class="avatar" style="background-image: url('<?php echo $comment['avatar']; ?>');"></div>
								<?php } ?>
								<div class="title">
									<?php if( $comment['name'] ) { ?><h6><?php echo $comment['name']; ?></h6><?php } 
									$rate = ' rate__'.$comment['rates']; ?>
									<div class="rates<?php echo $rate; ?>"><span></span><span></span><span></span><span></span><span></span></div>
									<?php if( $comment['date'] ) { ?><div class="dates"><?php echo $comment['date']; ?></div><?php } ?>
								</div>
							</div>
							<?php if( $comment['comment'] ) { ?>
							<div class="body"><?php echo $comment['comment']; ?></div>
							<?php } ?>
						</div>
					<?php if( $comment['comment_url'] ) { ?></a><?php } ?>
				<?php } ?>
				</div>
				<?php 
				$link = get_sub_field('comments_link');
				if( $link ) { 
					$target = $link['target'] ? ' target="_blank' : ''; ?>
				<a class="text__link" href="<?php echo $link['url']; ?>"<?php echo $target; ?>><?php echo $link['title']; ?></a>
				<?php } ?>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</section>
<section class="services__packages__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="title text-center" data-aos="fade-up" data-aos-duration="600">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
		<?php } 
		$rows = get_sub_field('packages_row'); 
		foreach ( $rows as $row ) { ?>
		<div class="row">
			<div class="col">
				<div class="row__section">
					<?php if( $row['title'] ) { ?>
					<div class="row justify-content-center">
						<div class="col-md-6">
							<div class="row__title text-center" data-aos="fade-up" data-aos-duration="600">
								<h4><?php echo $row['title']; ?></h4>
							</div>
						</div>
					</div>
					<?php } 
					if( $row['packages'] ) { ?>
					<div class="row">
						<?php foreach ( $row['packages'] as $package ) { ?>
						<div class="col-md-6 col-lg-4">
							<form class="package" onsubmit="return false;" data-aos="fade-up" data-aos-duration="600">
								<div class="title">
									<?php if( $package['title'] ) { ?><h4><?php echo $package['title']; ?></h4><?php } ?>
									<?php if( $package['text'] ) { ?><p><?php echo $package['text']; ?></p><?php } ?>
								</div>
								<?php if( !$package['simple'] ) { ?>
									<div class="documents">
										<?php if( $package['documents_title'] ) { ?>
										<div class="title"><?php echo $package['documents_title']; ?></div>
										<?php }
										$documents = $package['1c_documents_price'];
										if( $documents ) { ?>
										<div class="label__group">
											<label class="document__radio active">
												<span>50</span>
												<input type="radio" name="document" data-id="50" value="<?php echo $documents['50_documents']; ?>" checked>
											</label>
											<label class="document__radio">
												<span>100</span>
												<input type="radio" name="document" data-id="100" value="<?php echo $documents['100_documents']; ?>">
											</label>
											<label class="document__radio">
												<span>150</span>
												<input type="radio" name="document" data-id="150" value="<?php echo $documents['150_documents']; ?>">
											</label>
											<label class="document__radio">
												<span>200</span>
												<input type="radio" name="document" data-id="200" value="<?php echo $documents['200_documents']; ?>">
											</label>
										</div>
										<?php } ?>
									</div>
								<?php } ?>
								<div class="monthes">
									<?php if( $package['month_title'] ) { ?>
									<div class="title"><?php echo $package['month_title']; ?></div>
									<?php }
									$packages_price = $package['package_price']; 
									if( $packages_price['1_month_price'] ) { ?>
									<input type="text" name="per-month" value="<?php echo $packages_price['1_month_price']; ?>" required readonly hidden>
									<?php } ?>
									<div class="label__group">
										<label class="month__radio">
											<span>1</span>
											<input type="radio" name="month" data-id="1" value="0">
										</label>
										<label class="month__radio">
											<span>2</span>
											<input type="radio" name="month" data-id="2" value="<?php echo $packages_price['2_month_discount']; ?>">
										</label>
										<label class="month__radio">
											<span>3</span>
											<input type="radio" name="month" data-id="3" value="<?php echo $packages_price['3_month_discount']; ?>">
										</label>
										<label class="month__radio active">
											<span>6</span>
											<input type="radio" name="month" data-id="6" value="<?php echo $packages_price['more_than_6_month_discount']; ?>" checked>
										</label>
									</div>
								</div>
								<div class="summary">
									<div class="price__block">
										<div class="discounts">
											<div class="discount">-<span>10</span>%</div>
											<div class="discount__amount"><span>1990</span><?php _e(' hrn/month', 'zrobleno'); ?></div>
										</div>
										
										<?php 
										$discount = $package['package_discount'];
										if( $discount ) { ?>
										<input type="text" name="package-discount" value="<?php echo $package['package_discount']; ?>" required readonly hidden>
										<div class="price"><span>990</span><sup>*</sup><?php _e(' hrn/month', 'zrobleno'); ?></div>
										<div class="price__details">
											<div class="month"><?php _e('Price for ', 'zrobleno'); ?><span>1</span><?php _e(' month.', 'zrobleno'); ?></div>
											<?php if( !$package['simple'] ) { ?><div class="operations"><?php _e('Up to ', 'zrobleno'); ?><span>50</span><?php _e(' operations', 'zrobleno'); ?></div><?php } ?>
											<div class="details"><?php echo $package['discount_description']; ?> <span></span><?php _e(' hrn/month', 'zrobleno'); ?></div>
										</div>
										<?php } else { ?>
										<div class="price"><span>990</span><?php _e(' hrn/month', 'zrobleno'); ?></div>
										<div class="price__details">
											<div class="month"><?php _e('Price for ', 'zrobleno'); ?><span>1</span><?php _e(' month.', 'zrobleno'); ?></div>
											<?php if( !$package['simple'] ) { ?><div class="operations"><?php _e('Up to ', 'zrobleno'); ?><span>50</span><?php _e(' operations', 'zrobleno'); ?></div><?php } ?>
										</div>
										<?php } ?>
									</div>
									<button class="btn primary"><span><?php _e('Leave request', 'zrobleno'); ?></span></button>
								</div>
								<?php if( $package['packages_description'] ) { ?>
								<div class="packages__details d-block d-md-none">
									<div class="show__button text__link"><?php _e('Show features', 'zrobleno'); ?></div>
									<div class="details"><?php echo $package['packages_description']; ?></div>
								</div>
								<?php } ?>
							</form>
						</div>
						<?php } ?>
					</div>
					<?php } 
					$show_table = $row['show_packages_table']; 
					if( $show_table ) { 
					$table = $row['tables']; ?>
					<div class="row">
						<div class="col">
							<div class="compare__rows text-center" data-aos="fade-up" data-aos-duration="600">
								<button class="compare__button"><?php _e('Compare packages', 'zrobleno'); ?></button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<div class="packages__table">
								<?php if( $table['title'] ) { ?>
									<div class="title text-center"><h4><?php echo $table['title']; ?></h4></div>
								<?php } 
								$table_heads = $table['table_heads']; 
								if( $table_heads ) { ?>
								<div class="table__heads">
									<div class="cell title"><?php echo $table_heads['column_1']; ?></div>
									<div class="cell"><?php echo $table_heads['column_2']; ?></div>
									<div class="cell"><?php echo $table_heads['column_3']; ?></div>
									<div class="cell"><?php echo $table_heads['column_4']; ?></div>
								</div>
								<?php } 
								$table_rows = $table['table_rows'];
								if( $table_rows ) { ?>
								<div class="table__rows">
									<?php foreach ( $table_rows as $row ) { ?>
										<div class="table__row">
											<div class="cell title"><?php echo $row['row_title']; ?></div>
											<div class="cell">
												<?php if( $row['package_availability_1'] ) { ?>
													<span class="checked"></span>
												<?php } else { ?>
													<span class="line"></span>
												<?php } ?>
											</div>
											<div class="cell">
												<?php if( $row['package_availability_2'] ) { ?>
													<span class="checked"></span>
												<?php } else { ?>
													<span class="line"></span>
												<?php } ?>
											</div>
											<div class="cell">
												<?php if( $row['package_availability_3'] ) { ?>
													<span class="checked"></span>
												<?php } else { ?>
													<span class="line"></span>
												<?php } ?>
											</div>
										</div>
									<?php } ?>
								</div>
								<?php }	
								$table_footer = $table['table_footer']; 
								if( $table_footer ) { ?>
								<div class="table__footer">
									<div class="cell title"><?php echo $table_footer['row_title']; ?></div>
									<div class="cell"><?php echo $table_footer['package_price_1']; ?></div>
									<div class="cell"><?php echo $table_footer['package_price_2']; ?></div>
									<div class="cell"><?php echo $table_footer['package_price_3']; ?></div>
								</div>
<?php }	
								$table_footer = $table['table_footer_2']; 
								if( $table_footer ) { ?>
								<div class="table__footer">
									<div class="cell title"><?php echo $table_footer['row_title']; ?></div>
									<div class="cell"><?php echo $table_footer['package_price_1']; ?></div>
									<div class="cell"><?php echo $table_footer['package_price_2']; ?></div>
									<div class="cell"><?php echo $table_footer['package_price_3']; ?></div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>
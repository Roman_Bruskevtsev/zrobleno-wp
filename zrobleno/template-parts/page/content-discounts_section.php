<section class="discounts__section background__blue">
	<div class="container">
		<div class="row">
			<?php if( get_sub_field('title') ) { ?>
			<div class="col-md-12 col-lg-3">
				<div class="title__block" data-aos="fade-up" data-aos-duration="600">
					<h3><?php the_sub_field('title'); ?></h3>
				</div>
			</div>
			<?php } 
			if( get_sub_field('blocks') ) { 
				foreach ( get_sub_field('blocks') as $block ) { ?>
					<div class="col-md-4 col-lg">
						<div class="discount__block text-center" data-aos="fade-left" data-aos-duration="600">
							<h4 class="h2 discount"><?php echo $block['discount']; ?></h4>
							<p><?php echo $block['description']; ?></p>
						</div>
					</div>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
</section>
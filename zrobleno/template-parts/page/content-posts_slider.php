<section class="posts__slider__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="title" data-aos="fade-left" data-aos-duration="600">
					<?php if( get_sub_field('choose_category_to_show') ) { ?><a class="link" href="<?php echo get_category_link( get_field('choose_category_to_show') ); ?>"><?php } ?>
					<h2><?php the_sub_field('title'); ?></h2>
					<?php if( get_sub_field('choose_category_to_show') ) { ?></a><?php } ?>
				</div>
			</div>
		</div>
		<?php } 
		if( get_sub_field('choose_category_to_show') ) { 
			$args = array(
				'posts_per_page' 	=> -1,
				'cat'				=> get_sub_field('choose_category_to_show')
			);

			$query = new WP_Query( $args );
			if ( $query->have_posts() ) { ?>
			<div class="row">
				<div class="col">
					<div class="posts__wrapper">
						<div class="posts__slider swiper-container" data-aos="fade-up" data-aos-duration="600">
							<div class="swiper-wrapper">
							<?php while ( $query->have_posts() ) { $query->the_post(); ?>
								<div class="swiper-slide">
									<?php get_template_part( 'template-parts/post/content', 'thumbnail' ); ?>
								</div>
							<?php } ?>
							</div>
							<div class="swiper-pagination"></div>
						</div>
						<div class="swiper-button-next"></div>
	    				<div class="swiper-button-prev"></div>
					</div>
				</div>
			</div>
			<?php } wp_reset_postdata(); 
		} ?>
	</div>
</section>
<section class="employees__slider__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="title" data-aos="fade-left" data-aos-duration="600">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
		<?php } 
		$employees = get_sub_field('choose_employees'); 
		$args = array(
			'posts_per_page' 	=> -1,
			'post__in' 			=> $employees,
			'post_type'			=> 'employee',
			'orderby'			=> 'post__in '
		);

		$query = new WP_Query( $args );
		if ( $query->have_posts() ) { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="employees__slider swiper-container" data-aos="fade-up" data-aos-duration="600">
					<div class="swiper-wrapper">
					<?php while ( $query->have_posts() ) { $query->the_post(); ?>
						<div class="swiper-slide">
							<div class="employee">
							<?php
							if( get_field('avatar') ) { 
								$avatar = ' style="background-image: url('.get_field('avatar').')"'; ?>
								<div class="avatar"<?php echo $avatar; ?>></div>
							<?php } ?>
							<div class="text">
								<h6><?php the_title(); ?></h6>
								<?php the_field('details'); ?>	
							</div>
							</div>
						</div>
					<?php } ?>
					</div>
					<div class="swiper-pagination"></div>
					<div class="swiper-button-next"></div>
    				<div class="swiper-button-prev"></div>
				</div>
			</div>
		</div>
		<?php } wp_reset_postdata(); ?>
		</div>
	</div>
</section>
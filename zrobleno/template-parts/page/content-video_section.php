<section class="video__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="title text-center" data-aos="fade-up" data-aos-duration="600">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
		<?php } 
		$thumbnail = get_sub_field('video_thumbnail');
		$video_id = get_sub_field('video_id_youtube');
		?>
		<div class="row">
			<div class="col">
				<?php if( $video_id ) { ?>
				<div class="video__wrapper" data-aos="fade-up" data-aos-duration="600">
					<div class="video__block">
						<?php if( $thumbnail ) { ?>
							<div class="thumbnail" style="background-image: url(<?php echo $thumbnail; ?>)"></div>
						<?php } ?>
						<div class="play__btn"></div>
						<div class="video__frame">
							<iframe width="100%" height="100%" data-src="https://www.youtube.com/embed/<?php echo $video_id; ?>?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
						</div>
					</div>
				</div>
				<?php } ?>
				<div class="content__block text-center" data-aos="fade-up" data-aos-duration="600">
					<?php if( get_sub_field('icon') ) { ?>
					<div class="icon">
						<img src="<?php echo get_sub_field('icon')['url']; ?>" alt="<?php echo get_sub_field('icon')['title']; ?>">
					</div>
					<?php } ?>
					<div class="text">
						<?php if( get_sub_field('subtitle') ) { ?><h3 class="h2 dark-blue"><?php the_sub_field('subtitle'); ?></h3><?php } ?>
						<?php if( get_sub_field('text') ) { ?><p><?php the_sub_field('text'); ?></p><?php } ?>
					</div>
				</div>
			</div>	
		</div>
	</div>
</section>
<section class="requisites__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="title" data-aos="fade-left" data-aos-duration="600">
					<h3><?php the_sub_field('title'); ?></h3>
				</div>
			</div>
		</div>
		<?php } 
		$requisites_table = get_sub_field('requisites_table'); 
		if( $requisites_table ) { ?>
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="requisites__table" data-aos="fade-up" data-aos-duration="600">
				<?php foreach ( $requisites_table as $row ) { ?>
					<div class="req__row">
						<div class="th"><?php echo $row['title']; ?></div>
						<div class="td"><?php echo $row['text']; ?></div>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>
<section class="call__back__section background__dark">
	<div class="container">
		<div class="row">
			<div class="col-md-6 offset-md-2">
				<?php if( get_sub_field('title') ) { ?>
				<div class="title" data-aos="fade-left" data-aos-duration="600">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
				<?php }
				if( get_sub_field('shortcode') ) { ?>
				<div class="form__block" data-aos="fade-up" data-aos-duration="600">
					<?php echo do_shortcode( get_sub_field('shortcode') ); ?>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
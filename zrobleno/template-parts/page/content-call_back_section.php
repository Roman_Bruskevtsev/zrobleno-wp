<section class="call__back__section background__dark">
	<div class="container">
		<div class="row">
			<?php 
			$form = get_sub_field('form');
			$content = get_sub_field('content');
			if( $form ) { ?>
				<div class="col-md-6">
					<?php if( $form['title'] ) { ?>
					<div class="title" data-aos="fade-left" data-aos-duration="600">
						<h2><?php echo $form['title']; ?></h2>
					</div>
					<?php }
					if( $form['shortcode'] ) { ?>
					<div class="form__block" data-aos="fade-up" data-aos-duration="600">
						<?php echo do_shortcode($form['shortcode']); ?>
					</div>
					<?php } ?>
				</div>
			<?php } 
			if( $content ) { ?>
				<div class="col-md-5 offset-md-1">
					<div class="content text-center" data-aos="fade-left" data-aos-duration="600">
						<?php if( $content['icon'] ) { ?>
						<div class="icon">
							<img src="<?php echo $content['icon']['url']; ?>" alt="<?php echo $content['icon']['title']; ?>">
						</div>
						<?php } ?>
						<?php if( $content['title'] ) { ?><h3><?php echo $content['title']; ?></h3><?php } ?>
						<?php if( $content['text'] ) { ?><p><?php echo $content['text']; ?></p><?php } ?>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</section>
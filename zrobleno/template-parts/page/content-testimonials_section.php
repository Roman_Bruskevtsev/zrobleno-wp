 <section class="testimonials__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="title" data-aos="fade-left" data-aos-duration="600">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
		<?php } 
		$testimonials = get_sub_field('testimonials'); 
		if( $testimonials ) { 
			$args = array(
				'posts_per_page' 	=> 5,
				'post_type' 		=> 'testimonils',
				'post__in'			=> $testimonials
			);
			$query = new WP_Query( $args );
			if ( $query->have_posts() ) { ?>
				<div class="row">
					<div class="col">
						<div class="testimonials" data-aos="fade-up" data-aos-duration="600">
							<div class="row">
								<div class="col-md-12 col-lg-4">
									<div class="navigation">
									<?php 
									$i = 0;
									while ( $query->have_posts() ) { $query->the_post(); 
										$class = $i == 0 ? ' active' : '';
										$avatar = get_field('avatar') ? ' style="background-image: url('.get_field('avatar').')"' : ''; ?>
										<div class="test_avatar<?php echo $class; ?>">
											<div class="avatar"<?php echo $avatar; ?>></div>
											<div class="content d-none d-lg-block">
												<h6><?php the_field('owner'); ?></h6>
												<h6><?php the_field('company_type'); ?></h6>
												<h6><?php the_field('location'); ?></h6>
												<h6><?php the_field('company_name'); ?></h6>
											</div>
										</div>
									<?php $i++; } ?>
									</div>
								</div>
								<div class="col-md-12 col-lg-8">
									<div class="details">
									<?php 
									$style = get_sub_field('style');
									$i = 0;
									while ( $query->have_posts() ) { $query->the_post(); 
										if( $i == 0 ){
											if( $style ){
												get_template_part( 'template-parts/testimonial/content', 'testimonial-right-active' );
											} else {
												get_template_part( 'template-parts/testimonial/content', 'testimonial-active' );
											} 
										} else {
											if( $style ){
												get_template_part( 'template-parts/testimonial/content', 'testimonial-right' );
											} else {
												get_template_part( 'template-parts/testimonial/content', 'testimonial' );
											}
										}

									$i++; } ?>
									</div>
								</div>
							</div>
						</div>
						<?php if( get_sub_field('more_link_url') ) { ?>
						<div class="more__stories" data-aos="fade-left" data-aos-duration="600">
							<a href="<?php the_sub_field('more_link_url'); ?>" class="text__link"><?php the_sub_field('more_link_label'); ?></a>
						</div>
						<?php } ?>
					</div>
				</div>
			<?php } wp_reset_postdata();
		} ?>
	</div>
</section>
<?php 
$background = get_sub_field('background') ? ' style="background-image: url('.get_sub_field('background')['url'].')"' : '';
?>
<section class="page__banner background__grey"<?php echo $background; ?>>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="content">
					<?php if( get_sub_field('title') ) { ?>
					<div class="top__line" data-aos="fade-left" data-aos-duration="600">
						<h1><?php the_sub_field('title'); ?></h1>
						<?php if( get_sub_field('subtitle') ) { ?><p><?php the_sub_field('subtitle'); ?></p><?php } ?>
					</div>
					<?php }  
					$blocks = get_sub_field('blocks'); 
					if( $blocks ) { ?>
					<div class="bottom__line d-none d-lg-block">
					<?php 
					$delay = 150;
					foreach ($blocks as $block) { ?>
						<div class="block" data-aos="fade-up" data-aos-duration="600" data-aos-delay="<?php echo $delay; ?>"><?php echo $block['text']; ?></div>
					<?php $delay = $delay + 150; } ?>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php if( $blocks ) { ?>
<section class="page__banner__information background__white d-block d-lg-none">
	<div class="container-fluid">
		<div class="row">
			<div class="col">
				<div class="content">
				<?php $delay = 150;
					foreach ($blocks as $block) { ?>
					<div class="block" data-aos="fade-up" data-aos-duration="600" data-aos-delay="<?php echo $delay; ?>"><?php echo $block['text']; ?></div>
				<?php $delay = $delay + 150; } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>
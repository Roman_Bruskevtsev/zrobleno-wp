<?php 
$thumbnail = get_the_post_thumbnail_url( get_the_ID() ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'post-thumbnail' ).')"': ''; 
$commets = get_comments_number( get_the_ID() );
?>
<div class="col-md-6 col-lg-4 post__item webinar" data-aos="fade-up" data-aos-duration="600">
	<article id="webinar-<?php echo get_the_ID(); ?>" <?php post_class('post'); ?>>
			<div class="top__information">
				<h4><?php _e('Webinar', 'zrobleno'); ?></h4>
				<?php if( get_field('time') ) { ?>
					<time><?php the_field('time'); ?></time>
				<?php } 
				if( get_field('duration') ) { ?>
					<span><?php the_field('duration'); ?></span>
				<?php } ?>
			</div>
			<?php if( get_the_post_thumbnail_url( get_the_ID() ) ) { ?>
			<a href="<?php the_permalink(); ?>">
				<div class="image"<?php echo $thumbnail; ?>>
					<!-- <img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'post-thumbnail' ); ?>" alt="<?php the_title(); ?>"> -->
				</div>
			</a>
			<?php } ?>
			<div class="wrapper">
				<div class="text">
					<a href="<?php the_permalink(); ?>"><h5><?php the_title(); ?></h5></a>
				</div>
				<?php if( get_field('free') ) { ?>
					<div class="free"><?php _e('Free', 'zrobleno'); ?></div>
				<?php } 
				$lecturer = get_field('lecturer');
				if( $lecturer ) { ?>
				<div class="author">
					<?php if( $lecturer['avatar'] ) { 
						$avatar = ' style="background-image: url('.$lecturer['avatar'].');"';
					?>
					<div class="avatar"<?php echo $avatar; ?>></div>
					<?php } ?>
					<div class="information">
						<?php if( $lecturer['name'] ) { ?><h6><?php echo $lecturer['name']; ?></h6><?php } ?>
						<p>
							<?php echo $lecturer['position']; ?>
							<br><?php echo $lecturer['experience']; ?>	
						</p>
					</div>
				</div>
				<?php } ?>
				<div class="webinar__wrapper">
					<form class="webinar__form" onsubmit="return false;">
						<?php wp_nonce_field('security', 'form-security_'.wp_unique_id()); ?>
						<input type="text" name="webinar-id" value="<?php echo get_the_ID(); ?>" readonly hidden>
						<div class="field__group">
							<input type="email" name="email" class="form__field blue" placeholder="<?php _e('e-mail', 'zrobleno'); ?>" required>
						</div>
						<div class="submit__group">
							<input type="submit" class="form__submit blue" value="<?php _e('Sign up for a webinar', 'zrobleno'); ?>">
						</div>
						<?php 
						$webinars = new WebinarsClass();
						$amount = $webinars->get_subscribers_amount(get_the_ID());
						if( $amount > 0 ) { ?>
							<div class="subscribers"><?php echo $amount.__(' people signed up', 'zrobleno'); ?></div>
						<?php } ?>
					</form>
					<div class="webinar__success">
						<h5><?php _e('The link to the webinar has been sent to your email', 'zrobleno'); ?></h5>
						<p><?php _e('If the message is not in your Inbox, check your Spam folder', 'zrobleno'); ?></p>
					</div>
				</div>
			</div>
	</article>
</div>
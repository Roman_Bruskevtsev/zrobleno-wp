<?php 
if ( comments_open() || pings_open() ) { ?>
	<div class="comments__wrapper">
		<h4 data-aos="fade-left" data-aos-duration="600"><?php _e('Comments', 'zrobleno'); ?></h4>
		<?php 
		$comments = get_comments(); 
		if( $comments ) { ?>
		<div class="comments__list">
			<ul>
				<?php foreach ( $comments as $comment ) { 
					if( $comment->comment_approved ) { ?>
					<li data-aos="fade-up" data-aos-duration="600">
						<div class="comment__author">
							<h6><?php echo $comment->comment_author; ?></h6>
							<p><?php echo $comment->comment_date; ?></p>
						</div>
						<div class="comment__text">
							<p><?php echo $comment->comment_content; ?></p>
						</div>
					</li>
					<?php }
				} ?>
			</ul>
		</div>
		<?php } ?>
		<div class="comments__form" data-aos="fade-up" data-aos-duration="600">
			<?php 
			$commenter = wp_get_current_commenter();
			$consent = empty( $commenter['comment_author_email'] ) ? '' : ' checked="checked"';
			$args = array(
				'fields' => array(
					'author' => '
					<div class="field__group">
						<label>
							<span class="placeholder">'.__('Name *', 'zrobleno').'</span>
							<input class="form__field blue" id="author" name="author" type="text" value="'.esc_attr($commenter['comment_author']).'" size="30" required />
						</label>
					</div>',
					'email' => '
					<div class="field__group">
						<label>
							<span class="placeholder">'.__('Email *', 'zrobleno').'</span>
							<input class="form__field blue" id="email" name="email" type="email" value="'.esc_attr($commenter['comment_author_email']).'" size="30" aria-describedby="email-notes" required />
						</label>
					</div>',
					'url' => '
					<div class="field__group">
						<label>
							<span class="placeholder">'.__('Website', 'zrobleno').'</span>
							<input class="form__field blue" id="url" name="url" type="url" value="'.esc_attr( $commenter['comment_author_url'] ).'" size="30" />
						</label>
					</div>',
					'cookies' => '
					<div class="field__group checkbox">
						<label>
							<span class="placeholder">'.__( 'Save my name, email, and website in this browser for the next time I comment.', 'zrobleno') .'</span>
							'.sprintf( '<input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" value="yes"%s />', $consent ).'
						</label>
					</div>',
				),
				'comment_field' => '
				<div class="field__group">
					<label>
						<span class="placeholder">'.__( 'Comment *', 'zrobleno') .'</span>
						<textarea class="form__field blue" id="comment" name="comment" cols="45" rows="8" aria-required="true" required="required"></textarea>
					</label>
				</div>',
				'submit_field' => '<div class="submit__group">%1$s %2$s</div>',
				'class_submit' => 'form__submit blue'
			);
			comment_form($args); ?>
		</div>
	</div>
<?php }
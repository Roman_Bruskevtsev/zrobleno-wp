<?php
$author_id = get_the_author_meta('ID');
$avatar = get_avatar_url($author_id, array(
	'size'		=>	60,
	'default'	=> 'mystery'
) );
$thumbnail = get_the_post_thumbnail_url(get_the_ID(), 'full');
?>
<div class="author__block" data-aos="fade-up" data-aos-duration="600">
	<div class="row">
		<div class="col-md-6">
			<div class="information">
				<div class="avatar" style="background-image: url('<?php echo $avatar; ?>');"></div>
				<div class="name">
					<span><?php _e('Author', 'zrobleno'); ?></span>
					<h6><?php echo get_the_author_meta('first_name').' '.get_the_author_meta('last_name') ; ?></h6>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="share float-right">
				<span><?php _e('Share', 'zrobleno'); ?></span>
				<ul>
					<li class="st-custom-button messenger" data-network="messenger" data-title="<?php the_title(); ?>" data-image="<?php echo $thumbnail; ?>" data-description="<?php echo get_the_excerpt(); ?>"><?php _e('Facebook Massenger', 'zrobleno'); ?></li>
					<li class="st-custom-button telegram" data-network="telegram" data-title="<?php the_title(); ?>" data-image="<?php echo $thumbnail; ?>" data-description="<?php echo get_the_excerpt(); ?>"><?php _e('Telegram', 'zrobleno'); ?></li>
					<li class="st-custom-button whatsapp" data-network="whatsapp" data-title="<?php the_title(); ?>" data-image="<?php echo $thumbnail; ?>" data-description="<?php echo get_the_excerpt(); ?>"><?php _e('WhatsApp', 'zrobleno'); ?></li>
					<li class="st-custom-button skype" data-network="skype" data-title="<?php the_title(); ?>" data-image="<?php echo $thumbnail; ?>" data-description="<?php echo get_the_excerpt(); ?>"><?php _e('Skype', 'zrobleno'); ?></li>
				</ul>
			</div>
		</div>
	</div>
</div>
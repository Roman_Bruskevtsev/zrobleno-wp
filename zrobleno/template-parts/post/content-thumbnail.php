<a href="<?php the_permalink(); ?>"<?php post_class(); ?>>
	<?php $thumbnail = get_the_post_thumbnail_url( get_the_ID(), 'post-thumbnail' ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'post-thumbnail' ).')"': ''; ?>
	<div class="thumbnail"<?php echo $thumbnail; ?>></div>
	<h5><?php the_title(); ?></h5>
</a>
<div class="video__wrapper" data-aos="fade-up" data-aos-duration="600">
	<div class="video__block">
	<?php if( get_sub_field('video_thumbnail') ) { 
	$thumbnail = get_sub_field('video_thumbnail') ? ' style="background-image: url('.get_sub_field('video_thumbnail').')"' : ''; ?>
		<div class="thumbnail"<?php echo $thumbnail; ?>></div>
	<?php } ?>
	<?php if( get_sub_field('video_id_youtube') ) { ?>
		<div class="play__btn"></div>
		<div class="video__frame">
			<iframe width="100%" height="100%" data-src="https://www.youtube.com/embed/<?php the_sub_field('video_id_youtube'); ?>?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>
	<?php } ?>
	</div>
</div>
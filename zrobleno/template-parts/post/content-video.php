<?php 
$thumbnail = get_the_post_thumbnail_url( get_the_ID() ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'post-thumbnail' ).')"': ''; 
$commets = get_comments_number( get_the_ID() );
?>
<div class="col-md-6 col-lg-4 post__item video" data-aos="fade-up" data-aos-duration="600">
	<article id="post-<?php echo get_the_ID(); ?>" <?php post_class('post'); ?>>
		<?php if( get_the_post_thumbnail_url( get_the_ID() ) ) { ?>
		<a href="<?php the_permalink(); ?>">
			<div class="image"<?php echo $thumbnail; ?>>
				<!-- <img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'post-thumbnail' ); ?>" alt="<?php the_title(); ?>"> -->
			</div>
		</a>
		<?php } ?>
		<div class="text">
			<a href="<?php the_permalink(); ?>">
				<h5><?php the_title(); ?></h5>
			</a>
			<?php the_excerpt(); ?>
		</div>
		<div class="details">
			<span class="date"><?php echo get_the_date(); ?></span>
			<span class="comments"><?php echo $commets.__(' comments', 'zrobleno'); ?></span>
		</div>
	</article>
</div>
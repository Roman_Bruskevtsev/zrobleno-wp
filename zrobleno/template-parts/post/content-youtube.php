<?php 
$thumbnail = get_the_post_thumbnail_url( get_the_ID() ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'post-thumbnail' ).')"': ''; 
$commets = get_comments_number( get_the_ID() );
?>
<div class="col-md-6 col-lg-4 post__item youtube" data-aos="fade-up" data-aos-duration="600">
	<article id="post-<?php echo get_the_ID(); ?>" <?php post_class('post'); ?>>
		<?php if( get_the_post_thumbnail_url( get_the_ID() ) ) { ?>
		<div class="image"<?php echo $thumbnail; ?>>
			<!-- <img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'post-thumbnail' ); ?>" alt="<?php the_title(); ?>"> -->
		</div>
		<?php } ?>
		<div class="text">
			<h5><?php the_title(); ?></h5>
			<div class="excerpt">
				<?php the_excerpt(); ?>
			</div>
			<div class="text-center">
				<a href="<?php the_permalink(); ?>" class="btn secondary"><span><?php _e('Read', 'zrobleno'); ?></span></a>
			</div>
		</div>
	</article>
</div>
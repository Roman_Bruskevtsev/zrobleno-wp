<div class="related__block">
	<div class="row">
		<div class="col">
			<div class="related__title" data-aos="fade-left" data-aos-duration="600">
				<h2><?php _e('Related articles', 'zrobleno'); ?></h2>
			</div>
		</div>
	</div>
	<?php 
	$categories = get_the_category();

	if( $categories ) {
		$cat_array = [];
		foreach ( $categories as $cat ) {
			$cat_array[] = $cat->term_id;
		}

		$args = array(
	    	'post_type'			=> 'post',
	    	'post_status'		=> 'publish',
	    	'posts_per_page'	=> 6,
	    	'cat'				=> $cat_array,
	    	'post__not_in'		=> array(get_the_ID())
	    );

	    $query = new WP_Query( $args );

	    if ( $query->have_posts() ) {
	    	echo '<div class="row masonry">';
			while ( $query->have_posts() ) { $query->the_post();
				$view = get_field('view');

				switch ( $view ) {
					case '0':
						get_template_part( 'template-parts/post/content', 'default' );
						break;
					case '1':
						get_template_part( 'template-parts/post/content', 'facebook' );
						break;
					case '2':
						get_template_part( 'template-parts/post/content', 'youtube' );
						break;
					case '3':
						get_template_part( 'template-parts/post/content', 'video' );
						break;
					case '4':
						get_template_part( 'template-parts/post/content', 'webinar-default' );
						break;
					default:
						break;
				}
			}
			echo '</div>';
		} else {

		} wp_reset_postdata();
	} ?>
</div>
<?php 
$popup = get_field('popup', 'option');
if( $popup ) { ?>
<div class="popup" id="get-call-popup">
	<span class="close"></span>
	<div class="body">
		<?php if( $popup['title'] ) { ?><h4><?php echo $popup['title']; ?></h4><?php } 
		echo do_shortcode($popup['shortcode']); ?>
		<?php if( $popup['subtitle'] ) { ?><?php echo $popup['subtitle']; ?><?php } ?>
	</div>
	<?php if( $popup['bottom_line'] ) { ?><div class="bottom__line"><?php echo $popup['bottom_line']; ?></div><?php } ?>
</div>
<?php } ?>
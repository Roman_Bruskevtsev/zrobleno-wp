<?php
$thumbnail = get_field('video_thumbnail') ? ' style="background-image: url('.get_field('video_thumbnail').')"' : ''; ?>
<div class="test__content absolute">
	<div class="video__block">
		<?php if( get_field('video_thumbnail') ) { ?>
			<div class="thumbnail"<?php echo $thumbnail; ?>></div>
		<?php } ?>
		<?php if( get_field('video_id_youtube') ) { ?>
			<div class="play__btn"></div>
			<div class="video__frame">
				<iframe width="100%" height="100%" data-src="https://www.youtube.com/embed/<?php the_field('video_id_youtube'); ?>?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		<?php } ?>
	</div>
	
	<div class="content">
		<div class="project">
			<h5 class="d-block d-sm-none"><?php the_field('owner'); ?>, <?php the_field('company_type'); ?>, <?php the_field('location'); ?>, <?php the_field('company_name'); ?></h5>
			<?php the_field('company_plan'); ?>
			<?php if( get_field('price') ) { ?><b><?php the_field('price'); ?></b><?php } ?>
		</div>
		<?php if( get_field('description') ) { ?><div class="description"><?php the_field('description'); ?></div><?php } ?>
	</div>
</div>
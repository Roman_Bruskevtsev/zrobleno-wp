<?php 
$popup = get_field('get_package_popup', 'option');
if( $popup ) { ?>
<div class="popup medium" id="get-gackage-popup">
	<span class="close"></span>
	<div class="body__wrapper">
		<div class="left__side">
			<?php if( $popup['title'] ) { ?><h4><?php echo $popup['title']; ?></h4><?php } ?>
			<div class="package__details">
				<div class="name"><?php _e('You have selected a package ', 'zrobleno'); ?><span></span></div>
			</div>
		</div>
		<div class="right__side">
			<?php //echo do_shortcode($popup['shortcode']); ?>
			<?php echo get_locale() == 'uk' ? do_shortcode('[contact-form-7 id="2485"]') : do_shortcode('[contact-form-7 id="696"]'); ?>
			<?php if( $popup['form_subtitle'] ) { ?><div class="subtitle"><?php echo $popup['form_subtitle']; ?></div><?php } ?>
			<?php if( $popup['text'] ) { ?><div class="text"><?php echo $popup['text']; ?></div><?php } ?>
		</div>
	</div>
</div>
<?php } ?>
<?php
/**
 *
 * @package WordPress
 * @subpackage Zrobleno
 * @since 1.0
 * @version 1.0
 */
get_header(); 
$fag = get_field('faq', 'option'); 
if ( have_posts() ) {
	while ( have_posts() ) { the_post();
		$current_id = get_the_ID(); ?>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php get_template_part( 'template-parts/breadcrumbs' ); ?>
				</div>
			</div>
			<?php if( $fag['title'] ) { ?>
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<h1><?php echo $fag['title']; ?></h1>
					</div>
				</div>
			</div>
			<?php } 
			$args = array(
				'taxonomy' 		=> 'faq-categories',
				'hide_empty' 	=> false
			);
			$terms = get_terms( $args ); 
			if( $terms ) { ?>
			<div class="row">
				<div class="col-md-4">
					<div class="term__navigation">
						<ul>
						<?php foreach( $terms as $term ) { 
							$args = array(
								'posts_per_page' 	=> -1,
								'post_type'			=> 'faq',
								'tax_query'			=> array(
									array(
										'taxonomy' 	=> 'faq-categories',
										'field'    	=> 'term_id',
										'terms'		=> $term->term_id
									)
								)
							);
							$query = new WP_Query($args); 
							$class = is_object_in_term($current_id, 'faq-categories', $term->term_id) ? ' class="show"' : ''; ?>
							<li<?php echo $class; ?>>
								<span><?php echo $term->name; ?></span>
								<?php if ( $query->have_posts() ) { ?>
								<ul>
									<?php while ( $query->have_posts() ) { $query->the_post(); 
										$class = $current_id == get_the_ID() ? ' class="active"' : ''; ?>
										<li><a<?php echo $class; ?> href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
									<?php } ?>
								</ul>
								<?php } wp_reset_postdata(); ?>
							</li>
						<?php } ?>
						</ul>
					</div>
				</div>
				<div class="col-md-8">
					<div class="content">
						<h3 data-aos="fade-left" data-aos-duration="600"><?php the_title(); ?></h3>
						<?php if( get_field('video')['video_id_youtube'] ) { ?>
						<div class="video__wrapper" data-aos="fade-up" data-aos-duration="600">
							<div class="video__block">
							<?php if( get_field('video')['thumbnail'] ) { 
							$thumbnail = get_field('video')['thumbnail'] ? ' style="background-image: url('.get_field('video')['thumbnail'].')"' : ''; ?>
								<div class="thumbnail"<?php echo $thumbnail; ?>></div>
							<?php } ?>
								<div class="play__btn"></div>
								<div class="video__frame">
									<iframe width="100%" height="100%" data-src="https://www.youtube.com/embed/<?php echo get_field('video')['video_id_youtube']; ?>?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div>
							</div>
						</div>
						<?php } ?>
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	<?php }
}

get_footer();
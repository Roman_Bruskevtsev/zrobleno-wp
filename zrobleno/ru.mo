��    :      �      �      �  	   �  
   �     �     �     �     �                    *     >     N  	   U     _     h     y     �     �     �     �     �  	   �     �  
   �     �           	  ;   *     f  	   t     ~     �     �     �     �  
   �     �     �  M   �     '     .     4     B     X     `     l     �  *   �  3   �     �     �                    )     1     9  �  V     &	     @	     Q	     ^	  &   p	  %   �	     �	  2   �	     
  &   %
     L
  
   h
     s
     �
     �
  /   �
     �
  %   �
  9        U  B   h     �     �  )   �     �       /   $  r   T     �     �          #     ,     I     X     g     v     �  �   �     p     �  '   �  (   �  
   �  
   �  $   �     $  F   1  e   x     �     �  %   �          4     P     X     h    comments  hrn/month  month.  operations  people signed up Add Employee Add FAQ Add New Employee Add New FAQ Add New Testimonial Add Testimonial Author Comment * Comments Compare packages Edit Employee Edit FAQ Edit Testimonial Email not added Employee Employee information page Employees FAQ information page Follow us: Free Go to Here is a link to your webinar:  If the message is not in your Inbox, check your Spam folder Leave request Load more More webinars Name * New Employee New FAQ New Testimonial Price for  Read Related articles Save my name, email, and website in this browser for the next time I comment. Secure Share Show features Sign up for a webinar Success Testimonial Testimonial information page Testimonials Thank you for registering for our webinar! The link to the webinar has been sent to your email Up to  View View Employee View FAQ View Testimonial Webinar Website You have selected a package  Project-Id-Version: Zrobleno
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-09-07 14:48+0000
PO-Revision-Date: 2020-09-07 14:49+0000
Last-Translator: 
Language-Team: Russian
Language: ru
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.2; wp-5.5  комментариев  грн / мес  месяц.  операций  человека записались Добавить сотрудника Добавить FAQ Добавить нового сотрудника Добавить новый FAQ Добавить новый отзыв Добавить отзыв Автор Комментарий * Комментарии Сравнить пакеты Редактировать Сотрудника FAQ Редактировать Отзыв Электронная почта не добавлена Сотрудник Информационная страница сотрудника Сотрудники Страница FAQ Подписывайтесь на нас: Бесплатно Перейти к Вот ссылка на ваш вебинар: Если сообщения нет в папке «Входящие», проверьте папку «Спам». Оставьте запрос Показать больше Больше вебинаров Имя * Новый Cотрудник Новый FAQ Новый FAQ Цена за  Читать Статьи по теме Сохраните мое имя, адрес электронной почты и веб-сайт в этом браузере, чтобы в следующий раз я оставил комментарий. Безопасно Поделиться Показать особенности Записаться на вебинар Успех Отзыв Страница с отзывами Отзывы Спасибо за регистрацию на наш вебинар! Ссылка на вебинар отправлена на вашу электронную почту До  Посмотреть Просмотр Сотрудника Просмотр FAQ Просмотр Отзыв Webinar Веб-сайт Вы выбрали пакет  
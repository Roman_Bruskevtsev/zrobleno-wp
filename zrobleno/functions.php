<?php
/**
 *
 * @package WordPress
 * @subpackage Zrobleno
 * @since 1.0
 * @version 1.0
 */

if (!session_id()) {
    session_start();
}
$uri = parse_url(getenv('REQUEST_URI'));
if (isset($uri['query']) && !empty($uri['query'])) {
	parse_str( $uri['query'], $utm );
	if ( isset($utm['utm_source']) ) {
		$_SESSION['wp_marker_utm']  = $uri['query'];
		setcookie('wp_marker_utm', $uri['query'], time() + (86400 * 5), '/');
	}
}

add_action('init', 'getUTM', 1);
function getUTM(){
	if (!isset($_SESSION['wp_marker_utm']) && isset($_COOKIE['wp_marker_utm']) && !empty($_COOKIE['wp_marker_utm'])) {
		return "?" . $_COOKIE['wp_marker_utm'];
	} elseif(isset($_SESSION['wp_marker_utm']) && !empty($_SESSION['wp_marker_utm'])){
		return "?" . $_SESSION['wp_marker_utm'];
	}
}

if ( !isset($_SESSION['GATID']) ) $_SESSION['GATID'] = 'UA-100338745-3';



/*Theme settings*/
require get_template_directory() . '/inc/classes/theme-settings-class.php';
require get_template_directory() . '/inc/classes/forms-class.php';
require get_template_directory() . '/inc/classes/zrobleno-class.php';
require get_template_directory() . '/inc/classes/webinars-class.php';

function get_the_user_ip() {
	if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}

function get_the_ga_cookie() {
	return $_COOKIE['_ga'];
}

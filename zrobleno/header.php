<?php
/**
 *
 * @package WordPress
 * @subpackage Zrobleno
 * @since 1.0
 * @version 1.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-5TRM2MK');
    </script>
    <!-- End Google Tag Manager -->
    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '478630299512772');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=478630299512772&ev=PageView
        &noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body <?php body_class('load'); ?>>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5TRM2MK" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php wp_body_open();
    get_template_part( 'template-parts/header/preloader' ); ?>
    <header data-aos="fade-down" data-aos-duration="600">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <?php
                    $logo = get_field('logo', 'option');
                    if( $logo ) { ?>
                    <a class="logo float-left" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo $logo['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                    <?php } ?>
                    <?php if( has_nav_menu('main') ) {
                        wp_nav_menu( array(
                            'theme_location'        => 'main',
                            'container'             => 'nav',
                            'container_class'       => 'main__nav d-none d-lg-block float-left'
                        ) ); ?>
                    <?php } ?>
                    <div class="right__block float-right">
                    <?php if( get_field('phone_number', 'option') ) { ?>
                        <a href="tel:<?php the_field('phone_number', 'option'); ?>" class="float-left"><?php the_field('phone_number', 'option'); ?></a>
                    <?php }
                    if( get_field('call_back_label', 'option') ) { ?>
                        <button id="call-popup" class="btn transparent d-none d-lg-block float-left"><span><?php the_field('call_back_label', 'option'); ?></span></button>
                    <?php } ?>
                        <div class="menu__btn d-block d-lg-none float-left">
                            <span></span><span></span><span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="mobile__nav d-block d-lg-none">
        <div class="close__btn">
            <span></span>
            <span></span>
        </div>
        <?php if( has_nav_menu('main') ) {
            wp_nav_menu( array(
                'theme_location'        => 'mobile',
                'container'             => 'nav',
                'container_class'       => 'mobile__menu float-left'
            ) ); ?>
        <?php }
        $popup = get_field('popup', 'option');
        if( $popup ) { ?>
        <div class="form__wrapper">
            <div class="form__block">
                <?php if( $popup['title'] ) { ?><h5><?php echo $popup['title']; ?></h5><?php }
                echo do_shortcode($popup['shortcode']); ?>
                <?php if( $popup['subtitle'] ) { ?><?php echo $popup['subtitle']; ?><?php } ?>
            </div>
        </div>
        <?php }

        $languages = pll_the_languages(array('raw'=>1));
        if( $languages ){
            $current_lang = $lang_list = '';
            foreach ( $languages as $lang ) {
                if( $lang['current_lang'] ){
                    $current_lang = $lang['name'];
                } else {
                    $lang_list .= '<li><a href="'.$lang['url'].'">'.$lang['name'].'</a></li>';
                }
            } ?>
            <div class="language__switcher">
                <!-- <div class="current"><?php echo $current_lang; ?></div> -->
                <ul>
                    <?php echo $lang_list; ?>
                </ul>
            </div>
        <?php } ?>
    </div>
    <main>
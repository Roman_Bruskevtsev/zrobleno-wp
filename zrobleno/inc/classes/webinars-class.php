<?php 

class WebinarsClass extends ZroblenoClass {
	public function __construct(){
		$this->actions_init();
	}

	public function actions_init(){
		add_action( 'wp_ajax_sign_up_webinar', array( $this, 'sign_up_webinar' ) );
        add_action( 'wp_ajax_nopriv_sign_up_webinar', array( $this, 'sign_up_webinar' ) );

        add_filter( 'wp_mail_from_name', array( $this, 'email_sender_name' ) );
        add_filter( 'wp_mail_from', array( $this, 'sender_email' ) );
	}

	public function __return_false() {
        return false;
    }

    public function sign_up_webinar(){
    	if( wp_verify_nonce( reset($_POST), 'security' ) ) {
    		$webinar_id = $_POST['webinar-id'];

    		$webinar_emails = get_field('subscribers_email', $webinar_id);
            $send = $this->send_webinar_url($webinar_id, $_POST['email']);

    		if( $webinar_emails ){
    			$emails_array = explode(',', $webinar_emails);

    			if( in_array( $_POST['email'], $emails_array ) && $send ){
    				wp_send_json_success( __('Success', 'zrobleno'), 200 );
    			} else {
    				$webinar_emails = $webinar_emails.','.$_POST['email'];
    			}
    		} else {
    			$webinar_emails = $_POST['email'];
    		}
            
    		$field = update_field('subscribers_email', $webinar_emails, $webinar_id);

    		if( $field && $send ){
    			wp_send_json_success( __('Success', 'zrobleno'), 200 );
    		} else {
    			wp_send_json_error( __('Email not added', 'zrobleno'), 400 );
    		}
    	}
    }

    public function get_subscribers_amount($id){
    	$webinar_emails = get_field('subscribers_email', $id);
        $emails_array = explode(',', $webinar_emails);
        $count = get_field('subscribers_email', $id) ? count($emails_array) : 0;

    	return $count;
    }

    public function send_webinar_url($id, $email){
        $link = get_field('webinar_link', $id);

        $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
            <html>
            <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="profile" href="http://gmpg.org/xfn/11">
            </head>
            <body style="padding: 80px 0 120px; margin: 0; font-family: Roboto,RobotoDraft,Helvetica,Arial,sans-serif; background-color: #F5F5F5;">
                <table cellspacing="0" cellpadding="0" border="0" align="center" style="width:800px; max-width:800px; border: none; padding: 20px 50;"">
                    <table cellspacing="0" cellpadding="0" border="0" width="700" align="center" style="width:700px; padding: 45px 50px 35px; background-color: #fff;">
                        <thead>
                            <tr>
                                <th>
                                    <a href="https://zrobleno.com/" target="_blank" style="display: block; text-align: left;">
                                        <img src="'.get_theme_file_uri( '/assets/images/logo.png' ).'" width="180" height="54" alt="'.get_bloginfo('name').'" style="margin: 0 0 15px;">
                                    </a>
                                </th>
                                <th style="vertical-align: top;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="2">
                                    <h2 style="color: #2C2C2D; font-size: 24px; font-weight: 400; line-height: 32px; margin-top: 40px; margin-bottom: 10px">'.__('Thank you for registering for our webinar!', 'zrobleno').'</h2>
                                    <p style="font-size: 16px; line-height: 24px;">'.__('Here is a link to your webinar: ', 'zrobleno').'<a href="'.$link.'" target="_blank">'.$link.'</a></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table cellspacing="0" cellpadding="0" border="0" width="700" align="center" style="width:700px; padding: 25px 50px 30px; background-color: #333; border: none;">
                        <tfoot>
                            <tr>    
                                <td style="vertical-align: bottom;">
                                    <a style="text-align: left; text-decoration: none; color: #fff; font-size: 14px; font-weight: 400; line-height: 24px; margin-bottom: 10px; margin-top: 0;" href="tel:'.get_field('phone_number', 'option').'">'.get_field('phone_number', 'option').'</a>
                                </td>
                                <td style="vertical-align: bottom;">
                                    <h5 style="text-align: right; color: #fff; font-size: 14px; font-weight: 400; line-height: 20px; margin-bottom: 0px; margin-top: 0;">'.__('Follow us:', 'zrobleno').'</h5>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">
                                    <a style="text-align: left; text-decoration: none; color: #fff; font-size: 14px; font-weight: 400; line-height: 20px; margin-bottom: 10px; margin-top: 0;" href="tel:'.get_field('email', 'option').'">'.get_field('email', 'option').'</a>
                                </td>
                                <td>
                                    <ul style="margin: 10px 0 0; padding: 0; list-style: none; width: 100%; text-align: right;">
                                        <li style="display: inline-block; margin: 0 0 0 10px;">
                                            <a href="https://www.youtube.com/channel/UCNX__2iD--0XnhaGWOk8b3A" target="_blank">
                                                <img src="'.get_theme_file_uri( '/assets/images/youtube.png' ).'" width="40" height="40" alt="'.get_bloginfo('name').'">
                                            </a>
                                        </li>
                                        <li style="display: inline-block; margin: 0 0 0 10px;">
                                            <a href="https://www.facebook.com/Bukhhalterskyi.Servis.Zrobleno/" target="_blank">
                                                <img src="'.get_theme_file_uri( '/assets/images/facebook.png' ).'" width="40" height="40" alt="'.get_bloginfo('name').'">
                                            </a>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </table>
            </body>
            </html>';

            $headers = "Content-type: text/html; charset=utf-8 \r\n";  
            $headers .= 'From: welcome@zrobleno.com' . "\r\n";

            $mail = wp_mail( $email, __('Outsourcing of Accounting Services • Zrobleno', 'zrobleno'), $html, $headers );

            return $mail;
    }

    public function email_sender_name($original_email_from){
        return get_bloginfo('name');
    }

    public function sender_email(){
        return 'welcome@zrobleno.com';
    }
}
$webinars = new WebinarsClass();
<?php
/**
 * 
 */
class ThemeSettingsClass {
	const SCRIPTS_VERSION = '1.0.19';

	public function __construct(){
		$this->scriptsDir = get_theme_file_uri().'/assets/js';
        $this->stylesDir = get_theme_file_uri().'/assets/css';

		$this->actions_init();
	}

	public function actions_init(){
		add_action( 'wp_enqueue_scripts', array( $this, 'scripts_styles' ) );
		add_action( 'after_setup_theme', array( $this, 'theme_setup' ) );
		add_action( 'wp_footer',  array( $this, 'js_variables' ) );
		add_action( 'widgets_init', array( $this, 'widgets_init') );
		add_action( 'init', array( $this, 'custom_posts_init') );
		add_action( 'init', array( $this, 'custom_taxonomy_init') );

		add_filter( 'nav_menu_css_class', array( $this, 'wpdev_nav_classes' ), 10, 2 );
		add_filter( 'upload_mimes', array( $this, 'enable_svg_types'), 99 );
		add_filter( 'wpseo_json_ld_output', '__return_false' );
		add_filter( 'body_class', array( $this, 'header_transparent') );
	}

	public function scripts_styles() {
		wp_enqueue_style( 'zrobleno-css', $this->stylesDir.'/main.min.css' , '', self::SCRIPTS_VERSION);
    	wp_enqueue_style( 'zrobleno-style', get_stylesheet_uri() );

    	
    	wp_enqueue_script( 'google-maps-key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA0-FJOs84Jsnf9pZgsR37XpxvBJG33xYo&libraries=places', array( 'jquery' ), self::SCRIPTS_VERSION, true );
    	wp_enqueue_script( 'all-js', $this->scriptsDir.'/all.min.js', array( 'jquery' ), self::SCRIPTS_VERSION, true );

    	if( get_field('sharethis_api_key', 'option') && is_single() ) wp_enqueue_script( 'share-js', 'https://platform-api.sharethis.com/js/sharethis.js#property='.get_field('sharethis_api_key', 'option').'&product=custom-share-buttons&cms=sop', array( 'jquery' ), self::SCRIPTS_VERSION, true );
    }

    public function theme_setup(){
    	// if( ENV == 'dev' ) update_option( 'upload_url_path', 'https://stage.bcareagency.com/wp-content/uploads', true );

    	load_theme_textdomain( 'zrobleno' );
	    add_theme_support( 'automatic-feed-links' );
	    add_theme_support( 'title-tag' );
	    add_theme_support( 'post-thumbnails' );

	    add_image_size( 'webinar-thumbnail', 350, 260, true );
	    add_image_size( 'post-thumbnail', 360, 240, true );

		add_post_type_support( 'page', 'excerpt' );

		register_nav_menus( array(
	        'main'          	=> __( 'Main Menu', 'zrobleno' ),
	        'mobile'          	=> __( 'Mobile Menu', 'zrobleno' ),
	        'content-nav'       => __( 'Content Menu', 'zrobleno' )
	    ) );

	    if( function_exists('acf_add_options_page') ) {
		    $general = acf_add_options_page(array(
		        'page_title'    => __('Theme General Settings', 'zrobleno'),
		        'menu_title'    => __('Theme Settings', 'zrobleno'),
		        'redirect'      => false,
		        'capability'    => 'edit_posts',
		        'menu_slug'     => 'theme-settings',
		    ));
		}
    }

    public function widgets_init(){
    	register_sidebar( array(
	        'name'          => __( 'Footer 1 (menu block)', 'zrobleno' ),
	        'id'            => 'footer-1',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'zrobleno' ),
	        'before_widget' => '<section id="%1$s" class="widget %2$s contact__widget">',
	        'after_widget'  => '</section>',
	        'before_title'  => '<h4>',
	        'after_title'   => '</h4>',
	    ) );

	    register_sidebar( array(
	        'name'          => __( 'Footer 2 (menu block)', 'zrobleno' ),
	        'id'            => 'footer-2',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'zrobleno' ),
	        'before_widget' => '<section id="%1$s" class="widget %2$s contact__widget">',
	        'after_widget'  => '</section>',
	        'before_title'  => '<h4>',
	        'after_title'   => '</h4>',
	    ) );
    }

    public function custom_posts_init(){
		$post_labels = array(
			'name'					=> __('Testimonials', 'zrobleno'),
			'singular_name'			=> __('Testimonial', 'zrobleno'),
			'add_new'				=> __('Add Testimonial', 'zrobleno'),
			'add_new_item'			=> __('Add New Testimonial', 'zrobleno'),
			'edit_item'				=> __('Edit Testimonial', 'zrobleno'),
			'new_item'				=> __('New Testimonial', 'zrobleno'),
			'view_item'				=> __('View Testimonial', 'zrobleno')
		);

		$post_args = array(
			'label'               	=> __('Testimonials', 'zrobleno'),
			'description'        	=> __('Testimonial information page', 'zrobleno'),
			'labels'              	=> $post_labels,
			'supports'            	=> array( 'title'),
			'taxonomies'          	=> array( '' ),
			'hierarchical'       	=> false,
			'public'              	=> true,
			'show_ui'             	=> true,
			'show_in_menu'        	=> true,
			'has_archive'         	=> true,
			'can_export'          	=> true,
			'show_in_nav_menus'   	=> true,
			'publicly_queryable'  	=> true,
			'exclude_from_search' 	=> false,
			'query_var'           	=> true,
			'capability_type'     	=> 'post',
			'menu_position'			=> 3,
			'rewrite'				=> array(
				'slug'				=> 'testimonials'
			),
			'menu_icon'           	=> 'dashicons-universal-access-alt'
		);
		register_post_type( 'testimonils', $post_args );

		$post_labels = array(
			'name'					=> __('Employees', 'zrobleno'),
			'singular_name'			=> __('Employee', 'zrobleno'),
			'add_new'				=> __('Add Employee', 'zrobleno'),
			'add_new_item'			=> __('Add New Employee', 'zrobleno'),
			'edit_item'				=> __('Edit Employee', 'zrobleno'),
			'new_item'				=> __('New Employee', 'zrobleno'),
			'view_item'				=> __('View Employee', 'zrobleno')
		);

		$post_args = array(
			'label'               	=> __('Employee', 'zrobleno'),
			'description'        	=> __('Employee information page', 'zrobleno'),
			'labels'              	=> $post_labels,
			'supports'            	=> array( 'title'),
			'taxonomies'          	=> array( '' ),
			'hierarchical'       	=> false,
			'public'              	=> true,
			'show_ui'             	=> true,
			'show_in_menu'        	=> true,
			'has_archive'         	=> false,
			'can_export'          	=> true,
			'show_in_nav_menus'   	=> true,
			'publicly_queryable'  	=> true,
			'exclude_from_search' 	=> false,
			'query_var'           	=> true,
			'capability_type'     	=> 'post',
			'menu_position'			=> 3,
			'rewrite'				=> array(
				'slug'				=> 'employees'
			),
			'menu_icon'           	=> 'dashicons-admin-users'
		);
		register_post_type( 'employee', $post_args );

		$post_labels = array(
			'name'					=> __('FAQ', 'zrobleno'),
			'singular_name'			=> __('FAQ', 'zrobleno'),
			'add_new'				=> __('Add FAQ', 'zrobleno'),
			'add_new_item'			=> __('Add New FAQ', 'zrobleno'),
			'edit_item'				=> __('Edit FAQ', 'zrobleno'),
			'new_item'				=> __('New FAQ', 'zrobleno'),
			'view_item'				=> __('View FAQ', 'zrobleno')
		);

		$post_args = array(
			'label'               	=> __('FAQ', 'zrobleno'),
			'description'        	=> __('FAQ information page', 'zrobleno'),
			'labels'              	=> $post_labels,
			'supports'            	=> array( 'title', 'editor'),
			'taxonomies'          	=> array( '' ),
			'hierarchical'       	=> false,
			'public'              	=> true,
			'show_ui'             	=> true,
			'show_in_menu'        	=> true,
			'has_archive'         	=> true,
			'can_export'          	=> true,
			'show_in_nav_menus'   	=> true,
			'publicly_queryable'  	=> true,
			'exclude_from_search' 	=> false,
			'query_var'           	=> true,
			'capability_type'     	=> 'post',
			'menu_position'			=> 3,
			'rewrite'				=> array(
				'slug'				=> 'faq'
			),
			'menu_icon'           	=> 'dashicons-feedback'
		);
		register_post_type( 'faq', $post_args );
    }

    public function custom_taxonomy_init(){
		$taxonomy_labels = array(
			'name'                        => _x('FAQ categories', 'zrobleno'),
			'singular_name'               => _x('FAQ category', 'zrobleno'),
		);

		$taxonomy_rewrite = array(
			'slug'                  => 'faq-categories',
			'with_front'            => true,
			'hierarchical'          => true,
		);

		$taxonomy_args = array(
			'labels'              => $taxonomy_labels,
			'hierarchical'        => true,
			'public'              => true,
			'publicly_queryable'  => false,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'show_in_nav_menus'   => true,
			'show_tagcloud'       => true,
			'rewrite'             => $taxonomy_rewrite,
		);
		register_taxonomy( 'faq-categories', 'faq', $taxonomy_args );
    }

    public function header_transparent( $classes ){
    	if( is_page() ){
    		$transparent = get_field('header_transparent') ? 'header__transparent' : '';
    		$classes[] = $transparent;
    	}
    	return $classes;
    }

    public function wpdev_nav_classes( $classes, $item ) {
	    if( ( is_post_type_archive( 'project' ) || is_singular( 'project' ) )
	        && $item->title == 'Блог' ){
	        $classes = array_diff( $classes, array( 'current_page_parent' ) );
	    } elseif( is_singular( 'project' ) && $item->object == 'project' ){
	    	$classes[] = 'current_page_parent';
	    } elseif( is_tax('projects-categories') && $item->title == 'Блог' ){
	    	$classes = array_diff( $classes, array( 'current_page_parent' ) );
	    }

	    return $classes;
	}

    public function enable_svg_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}

	public function js_variables(){ ?>
		<script type="text/javascript">
	        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
	    </script>
	<?php }

	public function google_tag_manager() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/gtm' );
    	}
    }

    public function schema_org() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/schema' );
    	}
    }

    public function google_tag_manager_noscript() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/gtm', 'noscript' );
    	}
    }

    public function __return_false() {
        return false;
    }
}

$theme_settings = new ThemeSettingsClass();
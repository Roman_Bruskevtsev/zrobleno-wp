<?php 

class ZroblenoClass {
	const SCRIPTS_VERSION = '1.0.0';
	
	public function __construct(){
		$this->actions_init();
	}

	public function actions_init(){
		add_action( 'wp_ajax_load_posts', array( $this, 'load_posts' ) );
        add_action( 'wp_ajax_nopriv_load_posts', array( $this, 'load_posts' ) );
	}

	public function __return_false() {
        return false;
    }

    public function load_posts(){
    	$paged = $_POST['page'];
    	$posts_per_page = get_option('posts_per_page');
    	$category = $_POST['category'];
    	$output = '';
    	$args = array(
			'post_type'			=> 'post',
			'posts_per_page' 	=> $posts_per_page,
			'post_status'		=> 'publish',
			'paged'				=> $paged
		);

		if($category != '*') $args['cat'] = $category;

		$query = new WP_Query( $args );
		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) { $query->the_post();
				$view = get_field('view');
				switch ( $view ) {
					case '0':
						get_template_part( 'template-parts/post/content', 'default' );
						break;
					case '1':
						get_template_part( 'template-parts/post/content', 'facebook' );
						break;
					case '2':
						get_template_part( 'template-parts/post/content', 'youtube' );
						break;
					case '3':
						get_template_part( 'template-parts/post/content', 'video' );
						break;
					case '4':
						get_template_part( 'template-parts/post/content', 'webinar-default' );
						break;
					default:
						break;
				}
			} 
		}
		wp_reset_postdata();
		wp_die();
    }
}

$zrobleno = new ZroblenoClass();
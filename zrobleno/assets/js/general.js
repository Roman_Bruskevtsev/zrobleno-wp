'use strict';

class GeneralClass{
    
    constructor(){
        this.init();
    }

    init(){
        this.documentReady();
        this.windowLoad();
        this.windowScroll();
    }

    documentReady(){
        document.addEventListener('DOMContentLoaded', function(event) {
            
        });

        jQuery(document).ready(function($) {
            $('select').select2({minimumResultsForSearch: -1});
            $('.paroller').paroller(); 

            $('input[type="tel"]').mask('(000) 000 00 00', {placeholder: "(___) ___ __ __"});

            zrobleno.mobileNav();
            zrobleno.mouseMove();
            zrobleno.testimonialsSlider();
            zrobleno.webinarsForm();
            zrobleno.tabSlider();
            zrobleno.packagesPrice();
            zrobleno.clientsSlider();
            zrobleno.employeesSlider();
            zrobleno.postsSlider();
            zrobleno.googleMap();
            zrobleno.videoWrapper();
            zrobleno.imageSlider();
            zrobleno.faqArchive();
            zrobleno.popup();
            zrobleno.loadPosts();
        }); 
    }

    windowLoad(){
        (function($) {
            $(window).load( function(){ 
                var $container = $('.masonry');
                $container.masonry({
                    itemSelector: '.post__item'
                });

                AOS.init();
            });
        })(jQuery);
    }

    windowScroll(){
        let header = document.querySelector('header'),
            topScroll;

        window.addEventListener('scroll', function() {
            topScroll = window.scrollY;

            if( topScroll > 100 ){
                header.classList.add('white');
            } else {
                header.classList.remove('white');
            }
        });
    }

    preloader(){
        
    }

    mobileNav(){
        let mobileBtn = document.querySelector('.menu__btn'),
            mobileNav = document.querySelector('.mobile__nav'),
            closeBtn = mobileNav.querySelector('.close__btn');

        mobileBtn.addEventListener('click', function(){
            if( mobileBtn.classList.contains('show') ){
                mobileBtn.classList.remove('show');
                mobileNav.classList.remove('show');
            } else {
                mobileBtn.classList.add('show');
                mobileNav.classList.add('show');
            }
        });
        closeBtn.addEventListener('click', function(){
            mobileBtn.classList.remove('show');
            mobileNav.classList.remove('show');
        });
    }

    mouseMove(){
        // let body = document.querySelector('body'),
        //     cursor = body.querySelector('.cursor'),
        //     circle = cursor.querySelector('.circle'),
        //     dot = cursor.querySelector('.dot'),
        //     hoverElementArray = [
        //         'A',
        //         'BUTTON'
        //     ],
        //     mouseOverElement, mouseOverElementClasslist;

        // body.addEventListener('mousemove', e => {
        //     mouseOverElement = e.target.tagName;
        //     mouseOverElementClasslist = e.target.classList;
            
        //     if( hoverElementArray.includes(mouseOverElement) || mouseOverElementClasslist.contains('cursor__hover') ){
        //         cursor.classList.add('hover');
        //         cursor.classList.remove('scroll');
        //         circle.setAttribute('style', 'transform: matrix(1, 0, 0, 1, '+ e.pageX + ',' + e.pageY + ') scale(0.4)');
        //     } else if( mouseOverElementClasslist.contains('cursor__scroll') ){
        //         cursor.classList.add('scroll');
        //         cursor.classList.remove('hover');
        //         circle.setAttribute('style', 'transform: matrix(1, 0, 0, 1, '+ e.pageX + ',' + e.pageY + ') scale(0.5)');
        //     } else {
        //         cursor.classList.remove('hover');
        //         cursor.classList.remove('scroll');
        //         circle.setAttribute('style', 'transform: matrix(1, 0, 0, 1, '+ e.pageX + ',' + e.pageY + ') scale(1)');
        //     }
            
        //     dot.setAttribute('style', 'transform: matrix(1, 0, 0, 1, '+ e.pageX + ',' + e.pageY + ')');
        //     // circle.setAttribute('style', 'transform: matrix(1, 0, 0, 1, '+ e.pageX + ',' + e.pageY + ')');
        // });
    }

    testimonialsSlider(){
        let testimonials = document.querySelector('.testimonials');

        if( testimonials ){
            let navItems = testimonials.querySelectorAll('.test_avatar'),
                contentItems = testimonials.querySelectorAll('.test__content'),
                array = Array.prototype.slice.call(navItems),
                playButtons = testimonials.querySelectorAll('.play__btn');
                // slider = setInterval(sliderInit, 10000);

            navItems.forEach(function(item){
                item.addEventListener('click', function(e){
                    let index = array.indexOf(item);

                    navItems.forEach(function(item){
                        item.classList.remove('active');
                    });
                    navItems[index].classList.add('active');

                    contentItems.forEach(function(item){
                        item.classList.remove('active');
                    });
                    contentItems[index].classList.add('active');

                    playButtons.forEach(function(play){
                        let videoWrapper = play.closest('.video__block'),
                            iframe = play.closest('.video__block').querySelector('iframe');

                        iframe.removeAttribute('src');
                        videoWrapper.classList.remove('play');
                    });
                    sliderInit();
                    // clearInterval(slider);
                    // slider = setInterval(sliderInit, 10000);
                });
            });

            playButtons.forEach(function(play){
                play.addEventListener('click', function(){
                    let videoWrapper = play.closest('.video__block'),
                        iframe = play.closest('.video__block').querySelector('iframe'),
                        src = iframe.dataset.src;
                        
                    iframe.setAttribute('src', src);

                    // clearInterval(slider);
                    videoWrapper.classList.add('play');
                });
            });

            function sliderInit(){
                let sliderArray = Array.prototype.slice.call(navItems),
                    currentSlide = testimonials.querySelector('.test_avatar.active'),
                    currentIndex = sliderArray.indexOf(currentSlide);

                navItems.forEach(function(item){
                    item.classList.remove('active');
                });
                contentItems.forEach(function(item){
                    item.classList.remove('active');
                });
                    navItems[currentIndex].classList.add('active');
                    contentItems[currentIndex].classList.add('active');

                // if( currentIndex < (sliderArray.length - 1) ){
                //     navItems[currentIndex + 1].classList.add('active');
                //     contentItems[currentIndex + 1].classList.add('active');
                // } else {
                //     navItems[0].classList.add('active');
                //     contentItems[0].classList.add('active');
                // }
            }
        }
    }

    webinarsForm(){
        let forms = document.querySelectorAll('form.webinar__form');

        if(forms){
            forms.forEach(function(form){
                form.addEventListener('submit', function(){
                    let formData           = new FormData(form),
                        xhr                = new XMLHttpRequest(),
                        submitField        = form.querySelector('input[type=submit]'),
                        wrapper            = form.closest('.webinar__wrapper');

                    submitField.classList.add('load');

                    formData.append( 'action', 'sign_up_webinar' );
                    xhr.open('POST', ajaxurl, true);
                    xhr.onload = function () {
                        let responseArray = JSON.parse(this.response);

                        console.log(responseArray);

                        if (this.status >= 200 && this.status < 400) {
                            wrapper.classList.add('success');
                            form.remove();

                        } else if( this.status >= 400 && this.status < 500 ){
                            
                        } else {
                            
                        }
                    }
                    xhr.onerror = function() {
                        console.log('connection error');
                    };
                    xhr.send(formData);

                    submitField.classList.remove('load');
                });
            });
        }
    }

    tabSlider(){
        let tabs = document.querySelectorAll('.tabs'),
            tabsNav = document.querySelectorAll('.navigation .tab'),
            tabsContent = document.querySelectorAll('.content .tab'),
            sliders = document.querySelectorAll('.tab__slider');

        if(sliders){
            sliders.forEach(function(slider){
                var swiper = new Swiper(slider, {
                    slidesPerView: 1,
                    slidesPerColumn: 1,
                    spaceBetween: 0,
                    navigation: {
                        nextEl: slider.closest('.tab').querySelector('.swiper-button-next'),
                        prevEl: slider.closest('.tab').querySelector('.swiper-button-prev')
                    },
                    pagination: {
                        el: slider.closest('.tab').querySelector('.swiper-pagination')
                    },
                    breakpoints: {
                        767: {
                            slidesPerView: 2,
                            slidesPerColumn: 2,
                        }
                    }
                });
            });

            tabsNav.forEach(function(nav){
                nav.addEventListener('click', function(){
                    let array = Array.prototype.slice.call(tabsNav),
                        currentIndex = array.indexOf(nav);

                    tabsNav.forEach(function(nav){
                        nav.classList.remove('active');
                    });
                    tabsContent.forEach(function(tab){
                        tab.classList.remove('active');
                    });

                    tabsNav[currentIndex].classList.add('active');
                    tabsContent[currentIndex].classList.add('active');
                });
            });
        }
    }

    packagesPrice(){
        let packages = document.querySelectorAll('form.package'),
            compareButtons = document.querySelectorAll('.compare__button'),
            detailsButtons = document.querySelectorAll('.show__button'),
            documentsRadio = document.querySelectorAll('input[name="document"]'),
            monthesRadio = document.querySelectorAll('input[name="month"]');

        if( packages ){
            let popup = document.getElementById('get-gackage-popup'),
                wrapper = document.querySelector('.popup__wrapper');

            packages.forEach(function(pack){
                pack.addEventListener('submit', function(){
                    let name = pack.querySelector('.title h4').textContent,
                        documents = pack.querySelector('input[name="document"]:checked') ? pack.querySelector('input[name="document"]:checked').dataset.id : '',
                        month = pack.querySelector('input[name="month"]:checked').dataset.id,
                        // monthPrice = pack.querySelector('input[name="per-month"]').value,
                        popupNameLabel = popup.querySelector('.package__details .name span'),
                        nameField = popup.querySelector('input[name="package-name"]'),
                        documentField = popup.querySelector('input[name="documents-number"]'),
                        monthField = popup.querySelector('input[name="month-amount"]');

                    popupNameLabel.textContent = name;
                    nameField.value = name;
                    documentField.value = documents;
                    monthField.value = month;
                    // monthPriceField.value = monthPrice;

                    // console.log(name, documents, month, monthPrice);

                    popup.classList.add('show');
                    wrapper.classList.add('show');
                });
            });

            detailsButtons.forEach(function(button){
                button.addEventListener('click', function(){
                    let details = button.closest('.packages__details').querySelector('.details');
                    if( button.classList.contains('show') ){
                        button.classList.remove('show');
                        details.classList.remove('show');
                    } else {
                        button.classList.add('show');
                        details.classList.add('show');
                    }
                });
            });

            documentsRadio.forEach(function(radio){
                radio.addEventListener('change', function(){
                    let radioLabel = radio.closest('label'),
                        radioLabels = radio.closest('.label__group').querySelectorAll('.document__radio');

                    radioLabels.forEach(function(label){
                        label.classList.remove('active');
                    });
                    radioLabel.classList.add('active');

                    calculatePrice();
                });
            });

            monthesRadio.forEach(function(radio){
                radio.addEventListener('change', function(){
                    let radioLabel = radio.closest('label'),
                        radioLabels = radio.closest('.label__group').querySelectorAll('.month__radio');

                    radioLabels.forEach(function(label){
                        label.classList.remove('active');
                    });
                    radioLabel.classList.add('active');

                    calculatePrice();
                });
            });

            function calculatePrice(){
                let priceBlocks = document.querySelectorAll('form.package');

                priceBlocks.forEach(function(block){
                    let montnPrice = block.querySelector('input[name="per-month"]') ? block.querySelector('input[name="per-month"]').value : 0,
                        checkedDocuments = block.querySelector('input[name="document"]:checked') ? block.querySelector('input[name="document"]:checked').value : 0,
                        checkedMonth = block.querySelector('input[name="month"]:checked').value,
                        operationsValue = block.querySelector('input[name="document"]:checked') ? block.querySelector('input[name="document"]:checked').dataset.id : '',
                        operationsLabel = block.querySelector('input[name="document"]:checked') ? block.querySelector('.price__details .operations span') : '',
                        dicountLabel = block.querySelector('.discount span'),
                        packagePriceLabel = block.querySelector('.discount__amount span'),
                        packageDiscountPricleLabel = block.querySelector('.price__block .price span'),
                        packageDiscount = block.querySelector('input[name="package-discount"]'),
                        packageDiscountLabel = block.querySelector('.price__details .details span');

                    dicountLabel.textContent = checkedMonth;
                    packagePriceLabel.textContent = parseInt(montnPrice) + parseInt(checkedDocuments);
                    if( packageDiscount ){
                        let packagePrice = parseInt(checkedMonth) == 0 ? parseInt(montnPrice) + parseInt(checkedDocuments) : (parseInt(montnPrice) + parseInt(checkedDocuments)) - (parseInt(montnPrice) + parseInt(checkedDocuments)) * parseInt(checkedMonth) / 100;
                        packageDiscountPricleLabel.textContent = parseInt(packagePrice) - parseInt(packagePrice) * parseInt(packageDiscount.value) / 100;
                        packageDiscountLabel.textContent = packagePrice;
                    } else {
                        packageDiscountPricleLabel.textContent = parseInt(checkedMonth) == 0 ? parseInt(montnPrice) + parseInt(checkedDocuments) : (parseInt(montnPrice) + parseInt(checkedDocuments)) - (parseInt(montnPrice) + parseInt(checkedDocuments)) * parseInt(checkedMonth) / 100;
                    }
                    
                    
                    if( operationsLabel ) operationsLabel.textContent = operationsValue;
                });
            }

            calculatePrice();
        }
        if( compareButtons ){
            compareButtons.forEach(function(button){
                button.addEventListener('click', function(){
                    let table = button.closest('.row__section').querySelector('.packages__table');

                    if( table.classList.contains('show') ){
                        table.classList.remove('show');
                    } else {
                        table.classList.add('show');
                    }
                });
            });
        }
    }

    clientsSlider(){
        let sliders = document.querySelectorAll('.clients__slider');

        if( sliders ) {
            sliders.forEach(function(slider){
                var swiper = new Swiper(slider, {
                    slidesPerView: 1.5,
                    loop: true,
                    spaceBetween: 20,
                    pagination: {
                        el: slider.querySelector('.swiper-pagination'),
                        clickable: true
                    },
                    navigation: {
                        nextEl: slider.querySelector('.swiper-button-next'),
                        prevEl: slider.querySelector('.swiper-button-prev')
                    },
                    breakpoints: {
                        540: {
                            slidesPerView: 2,
                            spaceBetween: 20
                        },
                        767: {
                            slidesPerView: 3,
                            spaceBetween: 52
                        },
                        991: {
                            slidesPerView: 4,
                            spaceBetween: 52
                        },
                    }
                });
            });
        }
    }

    employeesSlider(){
        let sliders = document.querySelectorAll('.employees__slider');

        if( sliders ) {
            sliders.forEach(function(slider){
                var swiper = new Swiper(slider, {
                    slidesPerView: 1.5,
                    loop: true,
                    spaceBetween: 48,
                    pagination: {
                        el: slider.querySelector('.swiper-pagination'),
                        clickable: true
                    },
                    navigation: {
                        nextEl: slider.querySelector('.swiper-button-next'),
                        prevEl: slider.querySelector('.swiper-button-prev')
                    },
                    breakpoints: {
                        480: {
                            slidesPerView: 2,
                            spaceBetween: 20
                        },
                        767: {
                            slidesPerView: 3,
                            spaceBetween: 52
                        },
                        991: {
                            slidesPerView: 4,
                            spaceBetween: 40
                        },
                        1200: {
                            slidesPerView: 5,
                            spaceBetween: 48
                        },
                    }
                });
            });
        }
    }

    postsSlider(){
        let sliders = document.querySelectorAll('.posts__slider');

        if( sliders ) {
            sliders.forEach(function(slider){
                var swiper = new Swiper(slider, {
                    slidesPerView: 1,
                    loop: true,
                    spaceBetween: 30,
                    pagination: {
                        el: slider.querySelector('.swiper-pagination'),
                        clickable: true
                    },
                    navigation: {
                        nextEl: slider.closest('.posts__wrapper').querySelector('.swiper-button-next'),
                        prevEl: slider.closest('.posts__wrapper').querySelector('.swiper-button-prev')
                    },
                    breakpoints: {
                        576: {
                            slidesPerView: 2
                        },
                        991: {
                            slidesPerView: 3
                        }
                    }
                });
            });
        }
    }

    googleMap(){
        let map = document.querySelector('.map');
        if( map ){
            let latitude = map.dataset.lat,
                longitude = map.dataset.lng,
                zoom = parseInt(map.dataset.zoom),
                markerIcon = map.dataset.marker,
                myLatlng = new google.maps.LatLng(latitude, longitude),
                mapOptions = {
                    zoom: zoom,
                    disableDefaultUI: false,
                    scrollwheel: false,
                    zoomControl: true,
                    streetViewControl: true,
                    center: myLatlng
                },
                googleMap = new google.maps.Map(document.getElementById('google__map'), mapOptions),
                mark_location = new google.maps.LatLng(latitude, longitude);

                let marker = new google.maps.Marker({
                    position: mark_location,
                    map: googleMap,
                    icon: markerIcon
                });

                marker.setMap(googleMap);
        }
    }

    imageSlider(){
        let sliders = document.querySelectorAll('.image__slider');

        if( sliders ) {
            sliders.forEach(function(slider){
                var swiper = new Swiper(slider, {
                    slidesPerView: 'auto',
                    centeredSlides: true,
                    loop: true,
                    spaceBetween: 8,
                    noSwiping: true,
                    pagination: {
                        el: slider.querySelector('.swiper-pagination'),
                        clickable: true
                    }
                });
            });
        }
    }

    videoWrapper(){
        let wrappers = document.querySelectorAll('.video__wrapper');

        if( wrappers ){
            wrappers.forEach(function(wrapper){
                let videoBlock = wrapper.querySelector('.video__block'),
                    playButton = videoBlock.querySelector('.play__btn');
                    
                if( playButton ){
                    playButton.addEventListener('click', function(){ 
                        let iframe = playButton.closest('.video__block').querySelector('iframe'),
                            src = iframe.dataset.src;

                        iframe.setAttribute('src', src);

                        videoBlock.classList.add('play');
                    });
                }
            });
        }
    }

    faqArchive(){
        let terms = document.querySelectorAll('.term__navigation li span');

        if( terms ) {
            terms.forEach(function(term){
                term.addEventListener('click', function(){
                    let list = term.closest('li');
                    if ( list.classList.contains('show') ){
                        list.classList.remove('show');
                    } else {
                        list.classList.add('show');
                    }
                });
            });
        }
    }

    loadPosts(){
        let loadLine = document.querySelector('.load__row .load__more');

        if( loadLine ){
            let wrapper = document.querySelector('.row.masonry'),
                pages = parseInt(loadLine.dataset.pages),
                category = loadLine.dataset.category,
                currentPage = parseInt(loadLine.dataset.current),
                load = false,
                xhr = new XMLHttpRequest();

            loadLine.addEventListener('click', function(){
                if( pages > currentPage && !load ){
                    

                    loadLine.classList.add('load');
                    loadLine.dataset.current = currentPage++;
                    load = true;
                    xhr.open('POST', ajaxurl, true)
                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;');
                    xhr.onload = function () {
                        loadLine.classList.remove('load');
                        let res =  this.response;
                        // wrapper.insertAdjacentHTML('beforeEnd', res);
                        var $container = jQuery('.masonry'),
                            $items = jQuery(res);

                        $container.append( $items ).masonry( 'appended', $items ).masonry('layout');
                        
                        load = false;
                        
                        if( pages <= parseInt(loadLine.dataset.current) + 1 ) loadLine.remove();
                        AOS.init();
                    };
                    xhr.onerror = function() {
                        console.log('connection error');
                    };
                    xhr.send('action=load_posts' +
                             '&category=' + category +
                             '&page=' + currentPage);
                }
            });
        }
    }

    popup(){
        let wrapper = document.querySelector('.popup__wrapper');

        if( wrapper ){
            let popups = document.querySelectorAll('.popup'),
                closes = document.querySelectorAll('.popup .close'),
                getCallBtn = document.getElementById('call-popup'),
                getCallBtnFooter = document.getElementById('call-popup-footer'),
                getCallPopup = document.getElementById('get-call-popup');

            wrapper.addEventListener('click', function(){
                wrapper.classList.remove('show');
                popups.forEach(function(popup){
                    popup.classList.remove('show');
                });
            });

            closes.forEach(function(close){
                close.addEventListener('click', function(){
                    let popup = close.closest('.popup');

                    wrapper.classList.remove('show');
                    popup.classList.remove('show');
                });
            });

            getCallBtn.addEventListener('click', function(){
                wrapper.classList.add('show');
                getCallPopup.classList.add('show');
            });

            getCallBtnFooter.addEventListener('click', function(){
                wrapper.classList.add('show');
                getCallPopup.classList.add('show');
            });
        }
    }
}

let zrobleno = new GeneralClass();
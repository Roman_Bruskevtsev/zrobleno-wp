<?php
/**
 *
 * @package WordPress
 * @subpackage Zrobleno
 * @since 1.0
 * @version 1.0
 */
get_header(); 
$fag = get_field('faq', 'option'); ?>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php get_template_part( 'template-parts/breadcrumbs' ); ?>
			</div>
		</div>
		<?php if( $fag['title'] ) { ?>
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $fag['title']; ?></h1>
				</div>
			</div>
		</div>
		<?php } 
		$args = array(
			'taxonomy' 		=> 'faq-categories',
			'hide_empty' 	=> false
		);
		$terms = get_terms( $args ); 
		if( $terms ) { ?>
		<div class="row">
			<div class="col-md-4">
				<div class="term__navigation">
					<ul>
					<?php foreach( $terms as $term ) { 
						$args = array(
							'posts_per_page' 	=> -1,
							'post_type'			=> 'faq',
							'tax_query'			=> array(
								array(
									'taxonomy' 	=> 'faq-categories',
									'field'    	=> 'term_id',
									'terms'		=> $term->term_id
								)
							)
						);
						$query = new WP_Query($args); ?>
						<li>
							<span><?php echo $term->name; ?></span>
							<?php if ( $query->have_posts() ) { ?>
							<ul>
								<?php while ( $query->have_posts() ) { $query->the_post(); ?>
									<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
								<?php } ?>
							</ul>
							<?php } wp_reset_postdata(); ?>
						</li>
					<?php } ?>
					</ul>
				</div>
			</div>
			<div class="col-md-8">
				<div class="content">
					<?php if( $fag['banner'] ) { ?><img src="<?php echo $fag['banner']['url']; ?>" alt="<?php echo $fag['banner']['title']; ?>"><?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
<?php get_footer();
<?php
/**
 *
 * @package WordPress
 * @subpackage Zrobleno
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php get_template_part( 'template-parts/breadcrumbs' ); ?>
		</div>
	</div>
	<?php if( get_field('blog', 'option')['title'] ) { ?>
	<div class="row">
		<div class="col-md-12">
			<div class="blog__title" data-aos="fade-left" data-aos-duration="600">
				<h1><?php echo get_field('blog', 'option')['title']; ?></h1>
			</div>
		</div>
	</div>
	<?php }
	if( has_nav_menu('main') ) { ?>
	<div class="row">
		<div class="col-md-12" data-aos="fade-up" data-aos-duration="600">
        <?php wp_nav_menu( array(
            'theme_location'        => 'content-nav',
            'container'             => 'nav',
            'container_class'       => 'content__nav'
        ) ); ?>
    	</div>
    </div>
    <?php } 
    $paged = get_query_var('paged') ? get_query_var('paged') : 1;

    $category = get_queried_object();
    $args = array(
    	'post_type'			=> array('post'),
    	'post_status'		=> 'publish',
    	'paged'				=> $paged,
    	'cat'				=> $category->term_id,
    	'posts_per_page'	=> get_option('posts_per_page')
    );

    $query = new WP_Query( $args );

    if ( $query->have_posts() ) {
    	echo '<div class="row masonry">';
		while ( $query->have_posts() ) { $query->the_post();
			$view = get_field('view');

			switch ( $view ) {
				case '0':
					get_template_part( 'template-parts/post/content', 'default' );
					break;
				case '1':
					get_template_part( 'template-parts/post/content', 'facebook' );
					break;
				case '2':
					get_template_part( 'template-parts/post/content', 'youtube' );
					break;
				case '3':
					get_template_part( 'template-parts/post/content', 'video' );
					break;
				case '4':
					get_template_part( 'template-parts/post/content', 'webinar-default' );
					break;
				default:
					break;
			}
		}
		echo '</div>';
	} else {

	} wp_reset_postdata(); ?>
	<div class="row">
		<div class="col">
			<div class="load__row text-center">
				<?php if( $query->max_num_pages > 1 ) { ?>
				<div class="load__more btn black" data-current="1" data-pages="<?php echo $query->max_num_pages; ?>" data-category="*">
					<span><?php _e('Load more', 'zrobleno'); ?></span>
				</div>
				<?php } ?>
			</div>
			<!-- <div class="pagination__row text-center">
				<?php echo paginate_links( array(
			        'format'  			=> 'page/%#%',
			        'current' 			=> $paged,
			        'total'   			=> $query->max_num_pages,
			        'mid_size'        	=> 2,
			        'prev_next'       	=> true,
			        'prev_text'    		=> '<span></span>',
					'next_text'    		=> '<span></span>',
			        'show_all'		  	=> true
			    )); ?>
			</div> -->
		</div>
	</div>
</div>

<?php get_footer();
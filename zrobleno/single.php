<?php
/**
 *
 * @package WordPress
 * @subpackage Zrobleno
 * @since 1.0
 * @version 1.0
 */
get_header(); 

while ( have_posts() ) : the_post(); 
$commets = get_comments_number( get_the_ID() );
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php get_template_part( 'template-parts/breadcrumbs' ); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8 offset-md-2">
			<div class="post__title">
				<h1 class="h2"><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid full__width grey">
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<div class="post__details">
					<div class="date"><?php echo get_the_date(); ?></div>
					<span class="comments"><?php echo $commets.__(' comments', 'zrobleno'); ?></span>
				</div>
			</div>
			<div class="col-md-8">
				<div class="post__excerpt">
					<?php the_excerpt(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-8 offset-md-2">
		<?php if( have_rows('content') ):

			while ( have_rows('content') ) : the_row();

				if( get_row_layout() == 'content_editor' ): 
           			get_template_part( 'template-parts/post/content', 'content_editor' );
           		elseif( get_row_layout() == 'video' ): 
           			get_template_part( 'template-parts/post/content', 'video_block' );
           		endif;

			endwhile;

		endif; 
		$view = get_field('view'); 
		if( $view == '4' ) get_template_part( 'template-parts/post/content', 'webinar-post' );
		get_template_part( 'template-parts/post/content', 'author' );
		get_template_part( 'template-parts/post/content', 'comments' );
		?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php get_template_part( 'template-parts/post/content', 'related' ); ?>
		</div>
	</div>
</div>
	
<?php endwhile; 

get_footer();
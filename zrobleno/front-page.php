<?php
/**
 *
 * @package WordPress
 * @subpackage Zrobleno
 * @since 1.0
 * @version 1.0
 */

get_header(); 

if( have_rows('content') ):

	while ( have_rows('content') ) : the_row();

		if( get_row_layout() == 'home_page_banner' ): 
            get_template_part( 'template-parts/page/content', 'home_page_banner' );
        elseif( get_row_layout() == 'testimonials_section' ): 
            get_template_part( 'template-parts/page/content', 'testimonials_section' );
        elseif( get_row_layout() == 'how_to_start_section' ): 
            get_template_part( 'template-parts/page/content', 'how_to_start_section' );
        elseif( get_row_layout() == 'posts_section_webinars' ): 
            get_template_part( 'template-parts/page/content', 'posts_section_webinars' );
        elseif( get_row_layout() == 'made_in_numbers_section' ): 
            get_template_part( 'template-parts/page/content', 'made_in_numbers_section' );
        elseif( get_row_layout() == 'example_section' ): 
            get_template_part( 'template-parts/page/content', 'example_section' );
        elseif( get_row_layout() == 'services_packages_section' ): 
            get_template_part( 'template-parts/page/content', 'services_packages_section' );
        elseif( get_row_layout() == 'discounts_section' ): 
            get_template_part( 'template-parts/page/content', 'discounts_section' );
        elseif( get_row_layout() == 'clients_story_section' ):
            get_template_part( 'template-parts/page/content', 'clients_story_section' );
        elseif( get_row_layout() == 'clients_slider_section' ):
            get_template_part( 'template-parts/page/content', 'clients_slider' );
        elseif( get_row_layout() == 'leaders_words_section' ):
            get_template_part( 'template-parts/page/content', 'leaders_words_section' );
        elseif( get_row_layout() == 'page_banner_section' ):
            get_template_part( 'template-parts/page/content', 'page_banner_section' );
        elseif( get_row_layout() == 'employees_slider' ):
            get_template_part( 'template-parts/page/content', 'employees_slider' );
        elseif( get_row_layout() == 'video_section' ):
            get_template_part( 'template-parts/page/content', 'video_section' );
        elseif( get_row_layout() == 'key_principles_section' ):
            get_template_part( 'template-parts/page/content', 'key_principles_section' );
        elseif( get_row_layout() == 'call_back_section' ):
            get_template_part( 'template-parts/page/content', 'call_back_section' );
        elseif( get_row_layout() == 'posts_slider' ):
            get_template_part( 'template-parts/page/content', 'posts_slider' );
        elseif( get_row_layout() == 'social_networks_section' ):
            get_template_part( 'template-parts/page/content', 'social_networks_section' );
        elseif( get_row_layout() == 'contacts_section' ):
            get_template_part( 'template-parts/page/content', 'contacts_section' );
        elseif( get_row_layout() == 'image_slider' ):
            get_template_part( 'template-parts/page/content', 'image_slider' );
        elseif( get_row_layout() == 'faq_section' ):
            get_template_part( 'template-parts/page/content', 'faq_section' );
        elseif( get_row_layout() == 'requisites_section' ):
            get_template_part( 'template-parts/page/content', 'requisites_section' );
        elseif( get_row_layout() == 'form_section' ):
            get_template_part( 'template-parts/page/content', 'form_section' );
        elseif( get_row_layout() == 'content_editor' ):
            get_template_part( 'template-parts/post/content', 'content_editor' );
        endif;
	endwhile;

endif;

get_footer();